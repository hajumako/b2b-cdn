const
  argv = require('minimist')(process.argv.slice(2)),
  fs = require('fs'),
  path = require('path'),
  moment = require('moment'),
  promiseLimit = require('promise-limit'), //https://github.com/featurist/promise-limit
  csv = require('fast-csv'); //https://github.com/C2FO/fast-csv

makeDir = require('make-dir'), //https://github.com/sindresorhus/make-dir
  probe = require('probe-image-size'), //https://github.com/nodeca/probe-image-size
  sharp = require('sharp'), //https://github.com/lovell/sharp

  imagemin = require('imagemin'), //https://github.com/imagemin/imagemin
  pngquant = require('imagemin-pngquant'), // https://github.com/imagemin/imagemin-pngquant
  webp = require('imagemin-webp'), //https://github.com/imagemin/imagemin-webp

  _dLogFormat = 'YYMMDD_HHmmss',
  _dConsoleFormat = 'YYYY-MM-DD HH:mm:ss',
  _scriptLogPath = '../../../logs/webp/',
  _renderOldLogPath = '../../../logs/render/';

const isDirectory = source => fs.lstatSync(source).isDirectory();
let jobs = [],
  csvJobs = [];

let _base = [
  'E:/BazaRenderow/',
  'F:/Deweloperzy/Baza renderów/'
];

let _paths = {
  'Heaters': 'Heaters',
  'Odlewane': 'Accessories',
  'OEM': 'Radiators',
  'Outlet': 'Radiators',
  'Terma_Aktualne': 'Radiators',
  'Terma_Stare': 'Radiators',
  'Wieszaki': 'Accessories',
};

let
  cnt = 0,
  threads = 1,
  mode = 'from log',
  limit,
  lastCheckup,
  compressLog,
  errorLog,
  _s = moment();

if (argv.hasOwnProperty('h') || argv.hasOwnProperty('help') || argv.hasOwnProperty('?')) {
  console.log('\n ' +
    'webp.js is used for png image compression using \'tinify\' (tinypng) or \'pngquant\'\n ' +
    'and conversion to webp format using \'cwebp-bin\'\n\n ' +
    'parameters:\n' +
    '   --src\t- required, source folder or file\n' +
    '   --dest\t- destination folder (absolute or relative); default: \'compressed\' subfolder in \'src\'\n' +
    '   --log\t- path to folder with new renders log file (absolute); default: \'log\' subfolder in \'src\'\n' +
    '   --threads\t- maximum number of parallel threads; default: ' + threads + '\n' +
    '   --force\t- compress all images from \'src\' if not existing  in \'dest\' or newer');
} else if (argv.hasOwnProperty('src') && argv.hasOwnProperty('force')) {
  mode = 'all files';
  if (argv.hasOwnProperty('threads') && Number.isInteger(argv.threads) && argv.threads > 0) {
    threads = argv.threads;
  }
  limit = promiseLimit(threads);
  argv.src = path.normalize(argv.src);
  argv.dest = argv.hasOwnProperty('dest') ? path.normalize(argv.dest) : path.join(argv.src, 'compressed');

  makeDir(path.normalize(_scriptLogPath))
    .then(() => {
      return getLastCheckup();
    })
    .then((data) => {
      lastCheckup = data;
      console.log('----------------------');
      console.log('lastCheckup', lastCheckup.date);
      console.log('----------------------');
      return saveLastCheckup();
    })
    .then((data) => {
      getCompressLog();
      console.log('----------------------');
      console.log('started', _s.format(_dConsoleFormat));
      console.log('----------------------');
      return saveCompressLog();
    })
    .then(() => {
      recursiveReadDirs({
        path: argv.src,
        nest: 0
      });
    })
    .then(() => {
      return Promise.all(jobs);
    })
    .then(() => {
      let _f = moment();
      compressLog.dateFinished = _f.format();
      compressLog.duration = _f.diff(_s, 'seconds') + 's';
      return saveCompressLog();
    })
    .then(() => {
      lastCheckup.date = _s.format();
      return saveLastCheckup();
    })
    .then(() => {
      console.log('----------------------');
      console.log('finished', moment().format(_dConsoleFormat));
      console.log('converted', compressLog.total + ' files');
      console.log('duration', compressLog.duration);
      console.log('----------------------');
    })
    .catch(err => {
      console.log('error', err);
      compressLog.errors.push(err);
      saveCompressLog();
    });
} else if (argv.hasOwnProperty('src')) {
  if (argv.hasOwnProperty('threads') && Number.isInteger(argv.threads) && argv.threads > 0) {
    threads = argv.threads;
  }
  limit = promiseLimit(threads);
  argv.src = path.normalize(argv.src);
  argv.dest = argv.hasOwnProperty('dest') ? path.normalize(argv.dest) : path.join(argv.src, 'compressed');
  argv.log = argv.hasOwnProperty('log') ? path.normalize(argv.log) : path.join(argv.src, 'log');

  makeDir(path.normalize(_scriptLogPath))
    .then(() => {
      return getLastCheckup();
    })
    .then((data) => {
      lastCheckup = data;
      console.log('----------------------');
      console.log('lastCheckup', lastCheckup.date);
      console.log('----------------------');
      return saveLastCheckup();
    })
    .then(() => {
      getCompressLog();
      console.log('----------------------');
      console.log('started', _s.format(_dConsoleFormat));
      console.log('----------------------');
      return saveCompressLog();
    })
    .then(() => {
      return makeDir(path.normalize(_renderOldLogPath));
    })
    .then(() => {
      console.log('reading log folder...');
      readRenderLogs(argv.log);
      return Promise.all(csvJobs);
    })
    .then((datas) => {
      //move log file to another location
      moveLogFile(datas);
    })
    .then(() => {
      return Promise.all(jobs);
    })
    .then(() => {
      let _f = moment();
      compressLog.dateFinished = _f.format();
      compressLog.duration = _f.diff(_s, 'seconds') + 's';
      return saveCompressLog();
    })
    .then(() => {
      lastCheckup.date = _s.format();
      return saveLastCheckup();
    })
    .then(() => {
      console.log('----------------------');
      console.log('finished', moment().format(_dConsoleFormat));
      console.log('converted', compressLog.total + ' files');
      console.log('duration', compressLog.duration);
      console.log('----------------------');
    })
    .then(() => {
      if (compressLog.errors.length) {
        return getErrorLog();
      }
    })
    .then((data) => {
      if (data) {
        errorLog = data;
        errorLog.push({
          date: _s.format(),
          errors: compressLog.errors
        });
        saveErrorLog();
      }
    })
    .catch(err => {
      console.log('error', err);
      compressLog.errors.push(err);
      saveCompressLog();
    });
} else {
  console.log('missing \'src\' argument');
  console.log('type \'--h\' for help');
}

function recursiveReadDirs(data) {
  console.log('reading dir:', data.path, 'nest:', data.nest);
  fs.readdirSync(data.path).map(
    (name) => {
      let fullPath = path.join(data.path, name);
      let destDir = data.path.replace(argv.src, argv.dest);
      if (isDirectory(fullPath)) {
        let destSubdir = path.join(destDir, name);
        if (fs.existsSync(destSubdir)) {
          console.log('dir exists:', destSubdir);
        } else {
          jobs.push(limit(() => {
            console.log('create dir:', destSubdir);
            makeDir(destSubdir);
          }));
        }
        recursiveReadDirs({
          path: fullPath,
          nest: data.nest + 1
        });
      } else {
        jobs.push(limit(() => {
          if (checkFile(fullPath, destDir)) {
            console.log('\t' + ++cnt + ' file queued:', fullPath);
            return toPngAndWebp(fullPath, destDir, cnt);
          } else {
            console.log('skipped:', fullPath);
          }
        }));
      }
    }
  );
}

function readRenderLogs(logDir) {
  let cnt2 = 0;
  fs.readdirSync(logDir).map(
    (name) => {
      if (path.parse(name).ext === '.csv') {
        compressLog.log_files.push(name);
        csvJobs.push(limit(() => {
          return new Promise((res, rej) => {
            try {
              console.log('reading ' + name + ' log file');
              csv
                .fromPath(path.join(logDir, name), {delimiter: ';'})
                .on('data', function (data) {
                  if (data.length === 2) {
                    let relPath = data[1];
                    _base.forEach((base) => {
                      relPath = relPath.replace(base, '');
                    });
                    let parts = relPath.split('/');
                    relPath = relPath.replace(parts[0], _paths[parts[0]]);
                    let fullPath = path.join(argv.src, relPath);
                    let destDir = path.parse(path.join(argv.dest, relPath)).dir;
                    console.log('\t' + ++cnt2, destDir);
                    jobs.push(limit(() => {
                      console.log('\t' + ++cnt + ' file queued:', fullPath);
                      if (fs.existsSync(fullPath)) {
                        compressLog.files.push({
                          type: !fs.existsSync(fullPath) ? 'new' : 'updated',
                          file: fullPath,
                          dest: destDir
                        });
                        return toPngAndWebp(fullPath, destDir, cnt);
                      } else {
                        console.log(cnt + ' error: src file doesn\'t exist ' + fullPath);
                        compressLog.errors.push({
                          msg: 'src file doesn\'t exist',
                          file: fullPath,
                          dest: destDir,
                          no: cnt
                        });
                      }
                    }));
                  } else {
                    console.log(++cnt2, data[0], data[1]);
                  }
                })
                .on('end', function () {
                  res([path.join(logDir, name), true]);
                });
            } catch (err) {
              rej([path.join(logDir, name), false]);
            }
          });
        }));
      }
    }
  );
  if (csvJobs.length === 0) {
    console.log('0 log files found');
  }
}

function checkFile(file, dest) {
  let stats = fs.statSync(file);
  let compress = false;
  let destFile = path.join(dest, path.basename(file, path.extname(file)) + '.png');
  if (!fs.existsSync(destFile)) { // check if file exists (is new)
    compressLog.files.push({
      type: 'new',
      file: file,
      dest: dest
    });
    compress = true;
  } else if (moment(stats.mtime).valueOf() - moment(lastCheckup.date).valueOf() > 0) { // if file was updated
    compressLog.files.push({
      type: 'updated',
      file: file,
      dest: dest
    });
    compress = true;
  }
  if (compress) {
    compressLog.total++;
  }

  return compress;
}

function toPngAndWebp(srcFile, destDir, cnt) {
  let stream = fs.createReadStream(srcFile);
  return new Promise(function (res, rej) {
    let destFile = path.join(destDir, path.basename(srcFile));
    fs.unlink(destFile, function (err) {
      if (err) {
        //file doesn't exist
      }
      res(['removed', destFile]);
    });
  })
    .then(() => {
      return probe(stream);
    })
    .then(data => {
      // fix for 'EMFILE: too many open files'
      // https://github.com/nodejs/node-v0.x-archive/issues/8232
      // additional lib for fix https://github.com/jshttp/on-finished
      stream.destroy();
      if (data.mime !== 'image/png') {
        console.log('\t' + cnt + ' not a png file; converting to png');
        return sharp(srcFile)
          .png()
          .toBuffer()
          .then(data => {
            return imagemin.buffer(data, {
              plugins: [
                pngquant()
              ]
            });
          })
          .then(buffer => {
            return new Promise(function (res, rej) {
              let pngDestFile = path.join(destDir, path.basename(srcFile).replace(/(\.(\S*))$/g, '.png'));
              fs.writeFile(pngDestFile, buffer, function (err) {
                if (err) {
                  console.log('write error', err);
                  compressLog.errors.push(err);
                }
                res([{path: pngDestFile}]);
              });
            });
          });
      } else {
        return imagemin([srcFile], destDir, {
          plugins: [
            pngquant()
          ]
        });
      }
    })
    .then((data) => {
      console.log('\t' + cnt + ' pngquant compression done:', data[0].path);
      return imagemin([data[0].path], destDir, {
        plugins: [
          webp({
            method: 6,
            nearLossless: 85
          })
        ]
      });
    })
    .then((data) => {
      console.log('\t' + cnt + ' webp conversion done:', data[0].path);
    })
    .catch(err => {
      console.log(cnt, 'error', err);
      compressLog.errors.push(err);
    });
}

function moveLogFile(datas) {
  if (datas) {
    datas.forEach((data) => {
      if (data[1] === true) {
        jobs.push(limit(() => {
          return new Promise(function (res, rej) {
            try {
              let p = path.parse(data[0]);
              move(data[0], path.join(_renderOldLogPath, p.name + '_' + _s.format(_dLogFormat) + p.ext), (err) => {
                res(!err);
              });
            } catch (err) {
              rej(err);
            }
          });
        }));
      }
    });
  }
}

function getLastCheckup() {
  return new Promise(function (res, rej) {
    try {
      fs.readFile(path.normalize(path.join(_scriptLogPath, '_lastCheckup.json')), (err, data) => {
        if (err) {
          data = {
            date: 0
          };
        } else {
          data = JSON.parse(data);
        }
        res(data);
      });
    } catch (err) {
      rej(err);
    }
  });
}

function saveLastCheckup() {
  return new Promise(function (res, rej) {
    fs.writeFile(
      path.normalize(path.join(_scriptLogPath, '_lastCheckup.json')),
      JSON.stringify(lastCheckup, null, 2),
      (err) => {
        if (err) {
          console.log('error', err);
          rej(err);
        }
        res();
      });
  });
}

function getCompressLog() {
  return compressLog = {
    dateStarted: _s.format(),
    dateFinished: 0,
    mode: mode,
    log_files: [],
    threads: threads,
    duration: 0,
    total: 0,
    errors: [],
    files: []
  };
}

function saveCompressLog() {
  return new Promise((function (res, rej) {
    fs.writeFile(
      path.normalize(path.join(_scriptLogPath, 'webpLog_' + _s.format(_dLogFormat) + '.json')),
      JSON.stringify(compressLog, null, 2),
      (err) => {
        if (err) {
          console.log('error', err);
          rej(err);
        }
        res();
      });
  }));
}

function getErrorLog() {
  return new Promise(function (res, rej) {
    try {
      fs.readFile(path.normalize(path.join(argv.log, 'errorLog.json')), (err, data) => {
        if (err) {
          data = [];
        } else {
          data = JSON.parse(data);
        }
        res(data);
      });
    } catch (err) {
      rej(err);
    }
  });
}

function saveErrorLog() {
  return new Promise(function (res, rej) {
    fs.writeFile(
      path.normalize(path.join(argv.log, 'errorLog.json')),
      JSON.stringify(errorLog, null, 2),
      (err) => {
        if (err) {
          console.log('error', err);
          rej(err);
        }
        res();
      });
  });
}

function move(oldPath, newPath, callback) {
  copy();

  /*fs.rename(oldPath, newPath, function (err) {
    if (err) {
      if (err.code === 'EXDEV') {
        copy();
      } else {
        callback(err);
      }
      return;
    }
    callback();
  });*/

  function copy() {
    let readStream = fs.createReadStream(oldPath);
    let writeStream = fs.createWriteStream(newPath);

    readStream.on('error', callback);
    writeStream.on('error', callback);

    readStream.on('close', function () {
      //callback();
      fs.unlink(oldPath, callback);
    });

    readStream.pipe(writeStream);
  }
}