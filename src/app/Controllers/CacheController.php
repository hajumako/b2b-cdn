<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 22.03.2018
 * Time: 12:16
 */

namespace CDN\controllers;


use CDN\Classes\Helpers\MongoDb;
use CDN\Models\Cache;
use DateTime;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class CacheController extends Controller
{
  protected $cache;

  /**
   * CacheController constructor.
   * @param Container $container
   * @param Cache $cache
   */
  public function __construct(
    Container $container,
    Cache $cache
  )
  {
    $this->cache = $cache;
    parent::__construct($container);
  }

  /**
   * Update all records in mongo db
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function forcedUpdateMongo(Request $request, Response $response): Response
  {
    foreach ($this->cache->getCollectionDocs(MongoDb::COLLECTION_ACCESSORY_SPECS, []) as $bson) {
      $data = $bson->bsonSerialize();
      $this->cache->getAccessorySpecsFromAX($data->itemId, $data->configId);
    }
    foreach ($this->cache->getCollectionDocs(MongoDb::COLLECTION_COMMODITY_SPECS, []) as $bson) {
      $data = $bson->bsonSerialize();
      $this->cache->getCommoditySpecsFromAX($data->itemId, $data->configId);
    }
    foreach ($this->cache->getCollectionDocs(MongoDb::COLLECTION_HEATER_SPECS, []) as $bson) {
      $data = $bson->bsonSerialize();
      $this->cache->getHeaterSpecsFromAX($data->itemId, $data->configId);
    }
    foreach ($this->cache->getCollectionDocs(MongoDb::COLLECTION_RADIATOR_SPECS, []) as $bson) {
      $data = $bson->bsonSerialize();
      $this->cache->getRadiatorSpecsFromAX($data->itemId, $data->configId);
    }

    //getting data from AX upserts (update/insert) them
    $this->cache->getColorFromAX();

    return $response;
  }

  /**
   * Update records based on AX cache info
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function updateMongo(Request $request, Response $response): Response
  {
    //TODO finish when AX methods ready
    //1. get last cache change date from AX
    $cacheInfo = (object)array(
      'updated' => new DateTime(),
      'items' => [
        'a',
        'b',
        'c'
      ]
    );

    //2. compare with last date stored in mongo
    $data = $this->cache->getLastUpdateDate();
    if (!$data->lastUpdate || $data->lastUpdate->getTimeStamp < $cacheInfo->updated->getTimestamp()) {
      //3. update items from list
      //TODO
    }
    //4. set new update date
    $this->cache->setLastUpdateDate($cacheInfo->updated, 3);
    //5. store AX cache info
    $this->cache->saveAXCacheInfo($cacheInfo);

    return $response;
  }
}