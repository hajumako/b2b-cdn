<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:53
 */

namespace CDN\Classes\Traits;


trait CastFromParent
{
  /**
   * Method to load attributes from parent class object.
   *
   * @param $obj
   *   Source object or array
   */
  protected function castFromParent($obj)
  {
    $objValues = get_object_vars($obj);
    $objValues = $objValues ?: $obj;
    foreach ($objValues AS $key => $value) {
      $this->$key = $value;
    }
  }
}