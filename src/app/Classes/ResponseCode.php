<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 28.08.2017
 * Time: 11:20
 */

namespace CDN\Classes;

/**
 * @SWG\Definition(
 *   definition="ResponseCode",
 *   type="integer",
 *   enum={0,200,400,401,403,405,500,501,504},
 *   default=400
 * )
 */
interface ResponseCode
{
  const CONNECTION_ERROR = 0;
  const OK = 200;
  const ERROR = 400;
  const UNAUTHORIZED = 401;
  const FORBIDDEN = 403;
  const METHOD_NOT_ALLOWED = 405;
  const DB_EXCEPTION = 500;
  const EXCEPTION = 501;
  const WSDL_ERROR = 504;

  const errorCodes = [
    self::UNAUTHORIZED => [
      'name' => 'Unauthorized',
      'msg' => 'Unauthorized access'
    ],
    self::FORBIDDEN => [
      'name' => 'Forbidden',
      'msg' => 'Access denied'
    ],
    self::ERROR => [
      'name' => 'Error',
      'msg' => 'General error'
    ],
    self::METHOD_NOT_ALLOWED => [
      'name' => 'Method not allowed',
      'msg' => 'Unrecognized endpoint'
    ],
    self::EXCEPTION => [
      'name' => 'Exception',
      'msg' => 'Api Exception'
    ],
    self::DB_EXCEPTION => [
      'name' => 'Database exception',
      'msg' => ''
    ],
    self::WSDL_ERROR => [
      'name' => 'Soap Fault',
      'msg' => 'Connection to webservice failed'
    ]
  ];
}
