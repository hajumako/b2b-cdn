<?php

namespace CDN\Classes\Traits;

trait JsonSerializer
{
  /**
   * Recursive JSON serialization, used for object with protected/private properties
   *
   * @param bool $showLog
   *   Show log
   * @param int $l
   *   Recursion level
   *
   * @return array
   *   serialized object with camelCase properties
   */
  public function jsonSerialize(bool $showLog = false, int $l = 0): array
  {
    $serialized = array();
    foreach ($this as $prop => $value) {
      if ($showLog) {
        for ($i = 0; $i < $l; $i++) {
          echo "\t";
        }
        echo '<p style="margin:0 0 .2em '.$l.'em">'
          .$prop.':'
          .(gettype($value) === 'object' ? get_class($value) : gettype($value))
          .'</p>';
      }
      if (gettype($value) === 'object') {
        if (method_exists($value, 'jsonSerialize')) {
          $serialized[lcfirst($prop)] = $value->jsonSerialize($showLog, $l + 1);
        }
      } else {
        $serialized[lcfirst($prop)] = $value;
      }
    }

    return $serialized;
  }

  /**
   * Transform array to object & set values of properties
   * Does not support nesting of arrays of objects (cannot map property name to specific class)
   *
   * @param array $datas
   *   array of object properties
   */
  public function deserialize(array $datas)
  {
    foreach ($datas as $key => $value) {
      $key = ucfirst($key);
      $this->$key = $value;
    }
  }
}
