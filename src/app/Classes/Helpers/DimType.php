<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 17.01.2018
 * Time: 13:16
 */

namespace CDN\Classes\Helpers;


abstract class DimType
{
  const MODEL = 'model';
  const CAM_VIEW = 'cam_view';
  const DEF_VIEW = 'def_view';
  const MODEL_SIZE = 'model_size';
  const MODEL_COLOR = 'model_color';
  const IMAGE_WIDTH = 'image_width';
  const IMAGE_TYPE = 'image_type';
  const RADIATOR = 'radiator';
  const HEATER = 'heater';
  const ACCESSORY = 'accessory';
  const COMMODITY = 'commodity';
}