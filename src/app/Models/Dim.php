<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 17.01.2018
 * Time: 13:15
 */

namespace CDN\Models;

// TODO move to classes - not dependent on db
class Dim
{
  public $type;
  public $name;
  public $value;

  public function __construct(string $type, $value, string $name = null)
  {
    $this->type = $type;
    $this->name = $name;
    $this->value = $value;
  }
}