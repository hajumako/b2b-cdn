<?php

namespace CDN\Classes\Helpers;

/**
 * @SWG\Definition(
 *   definition="ProductGroupId",
 *   type="string",
 *   enum={"WYRG","WYRZ","WYRL","WYRE","WYRR","TOWG"},
 *   default="WYRG"
 * )
 */
abstract class ProductGroupId
{
  /**
   * AX constants - product groups.
   */
  const AX_PRODUCT_GROUP_NORMAL_RADIATORS = 'WYRG';
  const AX_PRODUCT_GROUP_IRON_RADIATORS = 'WYRZ';
  const AX_PRODUCT_GROUP_ELECTRIC_RADIATORS = 'WYRL';
  const AX_PRODUCT_GROUP_HEATERS = 'WYRE';
  const AX_PRODUCT_GROUP_ACCESSORIES = 'WYRR';
  const AX_PRODUCT_GROUP_COMMODITY = 'TOWG';

  /**
   * Get product group id by its short name
   *
   * @param string $groupId
   *   short name of product group id (ProductGroupIdShort)
   * @return string
   */
  public static function getGroupId(string $groupId): string
  {
    switch ($groupId) {
      case ProductGroupIdShort::AX_PRODUCT_GROUP_NORMAL_RADIATORS:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
      case ProductGroupIdShort::AX_PRODUCT_GROUP_IRON_RADIATORS:
        return self::AX_PRODUCT_GROUP_IRON_RADIATORS;
      case ProductGroupIdShort::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS:
        return self::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS;
      case ProductGroupIdShort::AX_PRODUCT_GROUP_HEATERS:
        return self::AX_PRODUCT_GROUP_HEATERS;
      case ProductGroupIdShort::AX_PRODUCT_GROUP_ACCESSORIES:
        return self::AX_PRODUCT_GROUP_ACCESSORIES;
      case ProductGroupIdShort::AX_PRODUCT_GROUP_COMMODITY:
        return self::AX_PRODUCT_GROUP_COMMODITY;
      default:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
    }
  }
}
