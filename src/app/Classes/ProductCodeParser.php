<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 01.02.2018
 * Time: 09:57
 */

namespace CDN\Classes;


use CDN\Classes\Helpers\MongoDb;
use CDN\Classes\Helpers\ProductGroupId;
use CDN\Models\Cache;
use CDN\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\RoutingService AS DTRoutingService;
use DI\Container;

class ProductCodeParser
{
  protected $container;
  protected $logger;
  protected $mongoDb;
  protected $B2BroutingService;
  protected $DTroutingService;
  protected $cache;

  protected $groupId;
  protected $groupShort;
  protected $subgroupShort;
  protected $itemId;
  protected $configId;

  //AX codes
  protected $code;
  protected $model;
  protected $modelShort;
  protected $size;
  protected $package;
  protected $colorId;
  protected $connectionId;
  protected $power;
  protected $cable;
  protected $other;

  //readable values
  protected $label;
  protected $height;
  protected $width;
  protected $color;
  protected $folder;

  //AX objects;
  protected $axSize;
  protected $axColor;

  protected $itemIdValid;
  protected $configIdValid;

  protected $pregMatches;

  // TODO should be taken from AX
  const PACKAGES = [
    'B',
    'F',
    'K',
    'P',
    'T',
    'X',
  ];

  const HEATER_COLORS = [
    '905' => 'black',
    '916' => 'white',
    'CRO' => 'chrome',
    'GPO' => 'gold',
    'SMA' => 'silver',
  ];

  const HEATER_NAME_FIND = [
    '/(Element grzejny|Grzałka)?(?: TERMA)? (?:(.+?) (OEM [^,]*)|(.*)), (\d{3,4}(?: )?W)/i',
    '/(Sterownik)?(?: TERMA)? (?:(.+?) (OEM [^,]*)|(.*))(.*)/i',
  ];

  const HEATER_NAME_RPLC = [
    'patterns' => [
      '/( (?:[+-]{1})(?:[ ]?)trójnik)/i',
      '/(30x40)/i',
      '/(30x30)/i',
      '/(36x40)/i',
      '/(lewy)/i',
      '/(prawy)/i',
      '/(3D)/i',
      '/([ -]{1})/i',
    ],
    'replacements' => [
      '_T',
      '',
      '',
      '',
      'LEFT',
      'RIGHT',
      '_3D',
      '_',
    ],
    'displayedName' => [
      ' + trójnik',
      '',
      '',
      '',
      'lewy',
      'prawy',
      ' 3D',
      ' ',
    ],
  ];

  const OTHERS_DIR = 'OTHERS';

  /**
   * Find product name in product label
   *
   * @param string $labelName
   * @return array
   */
  public static function extractHeaterName(string $labelName): array
  {
    $matches = [];
    foreach (self::HEATER_NAME_FIND as $pattern) {
      preg_match(
        $pattern,
        $labelName,
        $matches
      );
      if (count($matches)) {
        break;
      }
    }

    return $matches;
  }

  /**
   * Adjust heater name to CDN directory structure
   *
   * @param string $labelName
   * @return string
   */
  public static function normalizeHeaterName(string $labelName): string
  {
    return strtoupper(
      preg_replace(
        self::HEATER_NAME_RPLC['patterns'],
        self::HEATER_NAME_RPLC['replacements'],
        $labelName
      )
    );
  }

  /**
   * Get heater name for displaying
   *
   * @param string $prefix
   * @param string $labelName
   * @return string
   */
  public static function createHeaterDisplayedName(string $prefix, string $labelName): string
  {
    $name = preg_replace(
      self::HEATER_NAME_RPLC['patterns'],
      self::HEATER_NAME_RPLC['displayedName'],
      $labelName
    );

    return ($prefix === 'Sterownik' ? $prefix : '') . ' ' . $name;
  }

  /**
   * ProductCodeParser constructor.
   *
   * @param Container $container
   * @param B2BRoutingService $B2BroutingService
   * @param DTRoutingService $DTroutingService
   * @param Cache $cache
   */
  public function __construct(
    Container $container,
    B2BRoutingService $B2BroutingService,
    DTRoutingService $DTroutingService,
    Cache $cache
  ) {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->mongoDb = MongoDb::getConnection($this->container->get('mongodb'));
    $this->B2BroutingService = $B2BroutingService;
    $this->DTroutingService = $DTroutingService;
    $this->cache = $cache;
  }

  public function reset()
  {
    $this->groupId = null;
    $this->groupShort = null;
    $this->subgroupShort = null;
    $this->itemId = null;
    $this->configId = null;

    //AX codes
    $this->code = null;
    $this->model = null;
    $this->modelShort = null;
    $this->size = null;
    $this->package = null;
    $this->colorId = null;
    $this->connectionId = null;
    $this->power = null;
    $this->cable = null;
    $this->other = null;

    //readable values
    $this->label = null;
    $this->height = null;
    $this->width = null;
    $this->color = null;
    $this->folder = null;

    //AX objects;
    $this->axSize = null;
    $this->axColor = null;

    $this->itemIdValid = null;
    $this->configIdValid = null;
  }

  /**
   * @param string $code
   *
   * @return int
   */
  public function isAccessory(string $code)
  {
    return $this->runRegex('/(WR)(.{1})([^-\s]*)(?:-(\S*))?/', $code);
  }

  /**
   * @param string $code
   *
   * @return int
   */
  public function isCommodity(string $code)
  {
    return $this->runRegex('/(TG)([^-\s]*)(?:-(\S*))?/', $code);
  }

  /**
   * @param string $code
   *
   * @return int
   */
  public function isHeater(string $code)
  {
    return $this->runRegex(
      '/(WE)(\w{3})(\d{0,3})(?:[-]?)(' . implode('|', self::PACKAGES) . ')(\S{3})(\w{1})(\S*)/',
      $code
    );
  }

  /**
   * @param string $code
   *
   * @return int
   */
  public function isRadiator(string $code)
  {
    return $this->runRegex(
      '/(WG|WL|WS|WW|WZ)(\w{3})(\d{6})(?:[-]?)(' . implode('|', self::PACKAGES) . ')(\S{3})(\w{2,})/',
      $code
    );
  }

  /**
   * @param string $code
   */
  public function parseAccessory(string $code)
  {
    $this->code = $code;
    $codeArr = explode('-', $this->code);
    $this->itemId = $codeArr[0];
    $this->configId = array_key_exists(1, $codeArr) ? $codeArr[1] : null;

    if ($this->pregMatches) {
      $this->groupShort = $this->pregMatches[1];
      $this->subgroupShort = $this->pregMatches[2];
      if ($this->pregMatches[2] === 'W') {
        $this->model = $this->pregMatches[1] . $this->pregMatches[2] . substr($this->pregMatches[3], 0, 3);
      } else {
        $this->model = $this->pregMatches[1] . $this->pregMatches[2] . $this->pregMatches[3];
      }

      $this->groupId = ProductGroupId::getGroupId($this->groupShort);

      $this->itemIdValid = false;

      $specs = $this->cache->getAccessorySpecs($this->itemId, $this->configId);

      if (isset($specs->folder)) {
        $this->label = $specs->label;
        $this->folder = $specs->folder;
        $this->itemIdValid = true;
      }

      //not parsing colors
      $this->configIdValid = true;
    }
  }

  /**
   * @param $code
   */
  public function parseCommodity(string $code)
  {
    $this->code = $code;
    $codeArr = explode('-', $this->code);
    $this->itemId = $codeArr[0];
    $this->configId = array_key_exists(1, $codeArr) ? $codeArr[1] : null;

    if ($this->pregMatches) {
      $this->groupShort = $this->pregMatches[1];
      $this->model = $this->pregMatches[1] . $this->pregMatches[2];
      $this->colorId = $this->configId;

      $this->groupId = ProductGroupId::getGroupId($this->groupShort);

      $this->itemIdValid = false;

      $specs = $this->cache->getCommoditySpecs($this->itemId, $this->configId);

      if (
      isset($specs->folder)
      ) {
        $this->label = $specs->label;
        $this->folder = $specs->folder;
        $this->itemIdValid = true;
      }

      //not parsing colors
      $this->configIdValid = true;
    }
  }

  /**
   * @param string $code
   */
  public function parseHeater(string $code)
  {
    $this->code = $code;
    if ($this->pregMatches) {
      $this->itemId = $this->pregMatches[1] . $this->pregMatches[2] . $this->pregMatches[3];;//$codeArr[0];
      $this->configId = $this->configId = $this->pregMatches[4] . $this->pregMatches[5] . $this->pregMatches[6] . $this->pregMatches[7];//array_key_exists(1, $codeArr) ? $codeArr[1] : null;

      $this->groupShort = $this->pregMatches[1];
      $this->model = $this->pregMatches[1] . $this->pregMatches[2];
      $this->modelShort = $this->pregMatches[2];
      $this->power = $this->pregMatches[3];
      $this->package = $this->pregMatches[4];
      $this->colorId = $this->pregMatches[5];
      $this->cable = $this->pregMatches[6];
      $this->other = $this->pregMatches[7];

      $this->groupId = ProductGroupId::getGroupId($this->groupShort);

      $this->itemIdValid = false;

      $specs = $this->cache->getHeaterSpecs($this->itemId, $this->configId);

      if (isset($specs->folder)) {
        $this->label = $specs->label;
        $this->folder = $specs->folder;
        $this->itemIdValid = true;
      }

      if ($this->configId) {
        $color = self::HEATER_COLORS[$this->colorId];

        if ($color) {
          $this->color = $color;
          $this->configIdValid = true;
        }
      } else {
        $this->configIdValid = true;
      }
    }
  }

  /**
   * Parse radiator configuration based on SKU (itemId-configId)
   *
   * @param string $code
   *   Product SKU (itemId-configId)
   */
  public function parseRadiator(string $code)
  {
    $this->code = $code;
    if ($this->pregMatches) {
      $this->itemId = $this->pregMatches[1] . $this->pregMatches[2] . $this->pregMatches[3]; //$codeArr[0];
      $this->configId = $this->pregMatches[4] . $this->pregMatches[5] . $this->pregMatches[6]; //array_key_exists(1, $codeArr) ? $codeArr[1] : null;

      $this->groupShort = substr($this->itemId, 0, 2);
      $this->model = substr($this->itemId, 0, 5);
      $this->modelShort = substr($this->itemId, 2, 3);
      $this->size = substr($this->itemId, 5, 6);

      $this->groupId = ProductGroupId::getGroupId($this->groupShort);

      $this->itemIdValid = false;

      $specs = $this->cache->getRadiatorSpecs($this->itemId, $this->configId);

      if (
        isset($specs->height) &&
        isset($specs->width) &&
        isset($specs->label) &&
        isset($specs->folder)
      ) {
        $this->height = $specs->height;
        $this->width = $specs->width;
        $this->label = $specs->label;
        $this->folder = $specs->folder;
        $this->itemIdValid = true;
      }

      if ($this->configId) {
        $this->configIdValid = false;
        $this->package = substr($this->configId, 0, 1);
        $this->colorId = substr($this->configId, 1, 3);
        $this->connectionId = substr($this->configId, 4, 2);

        $color = $this->cache->getColor($this->colorId);

        if (
          isset($color->colorName) &&
          isset($color->axColor)
        ) {
          $this->color = $color->colorName;
          $this->axColor = is_array($color->axColor) ? (object)$color->axColor : $color->axColor->bsonSerialize();
          $this->configIdValid = true;
        }
      } else {
        $this->configIdValid = true;
      }
    }
  }

  public function isValid(): bool
  {
    return $this->itemIdValid && $this->configIdValid;
  }

  public function isItemIdValid(): bool
  {
    return $this->itemIdValid ?: false;
  }

  public function isConfigIdValid(): bool
  {
    return $this->configIdValid ?: false;
  }

  public function getCode(): string
  {
    return $this->code ?: '';
  }

  public function getGroupId(): string
  {
    return $this->groupId ?: '';
  }

  public function getGroupIdShort(): string
  {
    return $this->groupShort ?: '';
  }

  public function getSubgroupIdShort(): string
  {
    return $this->subgroupShort ?: '';
  }

  public function getItemId(): string
  {
    return $this->itemId ?: '';
  }

  public function getConfigId(): string
  {
    return $this->configId ?: '';
  }

  public function getModel(): string
  {
    return $this->model ?: '';
  }

  public function getModelShort(): string
  {
    return $this->modelShort ?: '';
  }

  public function getSize(): string
  {
    return $this->size ?: '';
  }

  public function getPackage(): string
  {
    return $this->package ?: '';
  }

  public function getColorId(): string
  {
    return $this->colorId ?: '';
  }

  public function getConnectionId(): string
  {
    return $this->connectionId ?: '';
  }

  public function getLabel(): string
  {
    return $this->label ?: '';
  }

  public function getHeight(): string
  {
    return $this->height ?: '';
  }

  public function getWidth(): string
  {
    return $this->width ?: '';
  }

  public function getColor(): string
  {
    return $this->color ?: '';
  }

  public function getFolder(): string
  {
    return $this->folder ?: '';
  }

  public function getAXcolor()
  {
    return $this->axColor;
  }

  public function getValues(): array
  {
    return array(
      'code' => $this->code,
      'groupId' => $this->groupId,
      'groupShort' => $this->groupShort,
      'itemId' => $this->itemId,
      'configId' => $this->configId,

      //AX codes
      'model' => $this->model,
      'modelShort' => $this->modelShort,
      'size' => $this->size,
      'package' => $this->package,
      'colorId' => $this->colorId,
      'connectionId' => $this->connectionId,
      'power' => $this->power,
      'cable' => $this->cable,
      'other' => $this->other,

      //readable values
      'label' => $this->label,
      'height' => $this->height,
      'width' => $this->width,
      'color' => $this->color,
      'folder' => $this->folder,

      //AX objects;
      'axSize' => $this->axSize,
      'axColor' => $this->axColor,

      'itemIdValid' => $this->itemIdValid,
      'configIdValid' => $this->configIdValid,
    );
  }

  /**
   * @param string $pattern
   * @param string $str
   *
   * @return int
   */
  protected function runRegex(string $pattern, string $str)
  {
    return preg_match(
      $pattern,
      $str,
      $this->pregMatches
    );
  }
}
