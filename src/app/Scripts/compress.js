const
  argv = require('minimist')(process.argv.slice(2)),
  fs = require('fs'),
  path = require('path'),
  moment = require('moment'),
  makeDir = require('make-dir'),
  sharp = require('sharp'), // https://github.com/lovell/sharp
  imagemin = require('imagemin'),
  pngquant = require('imagemin-pngquant'), // https://github.com/imagemin/imagemin-pngquant

  _dLogFormat = 'YYMMDD_',
  _dConsoleFormat = 'YYYY-MM-DD HH:mm:ss',
  _logPath = '../../logs/compress/';

let
  log,
  _s = moment(),
  _logName = 'compressLog_' + _s.format(_dLogFormat) + (argv.hasOwnProperty('hash') ? argv.hash : '');

if (argv.hasOwnProperty('src') && argv.hasOwnProperty('dest')) {
  argv.src = path.normalize(argv.src);
  argv.dest = path.normalize(argv.dest);

  makeDir(path.normalize(_logPath))
    .then(() => {
      log = {
        dateStarted: _s.format(),
        src: argv.src,
        dest: argv.dest
      };
    });

  if (!argv.hasOwnProperty('silent')) {
    console.log('----------------------');
    console.log('started', _s.format(_dConsoleFormat));
    console.log('----------------------');
  }

  makeDir(argv.dest)
    .then(() => {
      if(path.parse(argv.src).ext !== '.png')
        return convertToPng(argv.src, argv.dest);
    })
    .then(() => {
      console.log('compress');
      let src = path.join(argv.dest, path.parse(argv.src).name + '.png');
      return compressPngquant(src, argv.dest);
    })
    .then(data => {
      log.generated = data.length ? data[0].path : '----';
    })
    .then(() => {
      finish();
    })
    .catch(e => {
      if (!argv.hasOwnProperty('silent')) {
        console.log(e);
      }
    });
} else {
  if (!argv.hasOwnProperty('silent')) {
    if (!argv.hasOwnProperty('src')) {
      console.log('missing \'src\' argument');
    }
    if (!argv.hasOwnProperty('dest')) {
      console.log('missing \'dest\' argument');
    }
  }
}

function convertToPng(file, dest) {
  if (fs.lstatSync(dest).isDirectory()) { // destination is a directory; add file name to path
    dest = path.join(dest, path.parse(file).name + '.png');
  }

  return sharp(file)
    .toFile(dest);
}

function compressPngquant(file, dest) {
  if (!fs.lstatSync(dest).isDirectory()) { // destination is a directory; add file name to path
    dest = path.dirname(dest);
  }

  return imagemin([file], dest, {
    plugins: [
      pngquant()
    ]
  });
}

function saveLog() {
  fs.readFile(path.normalize(_logPath + _logName + '.json'), (err, data) => {
    let _log = [];
    if (err) {
      if (!argv.hasOwnProperty('silent')) {
        //console.log(err);
      }
    } else {
      _log = JSON.parse(data);
    }
    _log.push(log);
    fs.writeFile(
      path.normalize(_logPath + _logName + '.json'),
      JSON.stringify(_log, null, 2),
      (err) => {
        if (err) throw err;
      });
  });
}

function finish() {
  let _f = moment();
  log.dateFinished = _f.format();
  log.duration = _f.diff(_s, 'milliseconds') + 'ms';
  saveLog();

  if (!argv.hasOwnProperty('silent')) {
    console.log(log);
    console.log('----------------------');
    console.log('finished', moment().format(_dConsoleFormat));
    console.log('duration', log.duration);
    console.log('----------------------');
  }
}