<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.11.2017
 * Time: 13:20
 */

namespace CDN\Classes;


use CDN\Classes\Exceptions\ApiException;
use CDN\Classes\Exceptions\ForbiddenException;
use CDN\Classes\Exceptions\MethodNotAllowedException;
use CDN\Classes\Exceptions\UnauthorizedException;
use CDN\Models\ErrorModel;
use DI\Container;
use MongoDB\Driver\Exception\RuntimeException as MongoException;
use PDOException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Handlers\PhpError;
use SoapFault;
use Throwable;

class ErrorHandler extends PhpError
{
  protected $errorId;
  protected $errorModel;
  protected $allowedOrigins;
  protected $jwtPassthroughUrls;
  protected $testedUrls;
  protected $debugConfig;
  protected $logger;
  protected $allowAllOrigins;
  protected $isDebugMode;


  public function __construct(Container $container, ErrorModel $errorModel)
  {
    $this->errorId = self::getErrorId();
    $this->errorModel = $errorModel;
    $this->allowedOrigins = $container->get('allowed_origins');
    $this->logger = $container->get('logger');
    $this->debugConfig = $container->get('debug_config');
    $this->isDebugMode = $container->get('debug_config')['debug_mode'] ?? false;
    $this->allowAllOrigins = $container->get('debug_config')['allow_all_origins'] ?? false;

    parent::__construct(true);
  }

  public function __invoke(Request $request, Response $response, \Throwable $error): Response
  {
    if ($this->debugConfig['slim_handler'] ?? false) {
      return parent::__invoke($request, $response, $error);
    } else {
      switch (get_class($error)) {
        case UnauthorizedException::class:
          $code = ResponseCode::UNAUTHORIZED;
          break;
        case ForbiddenException::class:
          $code = ResponseCode::FORBIDDEN;
          break;
        case MethodNotAllowedException::class:
          $code = ResponseCode::METHOD_NOT_ALLOWED;
          break;
        case ApiException::class:
          $code = ResponseCode::EXCEPTION;
          break;
        //todo handle parent class
        case MongoException::class:
        case PDOException::class:
          $code = ResponseCode::DB_EXCEPTION;
          break;
        case SoapFault::class:
          $code = ResponseCode::WSDL_ERROR;
          break;
        default:
          $code = ResponseCode::ERROR;
      }

      return $this->errorResponse($request, $response, $error, $code);
    }
  }

  /**
   * Generate id for error
   *
   * @return string
   */
  public static function getErrorId(): string
  {
    return bin2hex(random_bytes(4)) . (new \DateTime())->getTimestamp();
  }

  /**
   * @param Request $request
   *   Psr request
   * @param Response $response
   *   Psr response
   * @param Throwable $error
   *  error message
   * @param int $code
   *   error code
   *
   * @return Response
   */
  protected function errorResponse(
    Request $request,
    Response $response,
    Throwable $error,
    int $code = 400
  ): Response {
    if ($this->debugConfig['log_exceptions'] ?? true) {
      $this->logger->addError($this->errorId . ' | ' . $error);
      $this->logger->addError('Error details:' . serialize($_SERVER));
    }

    $origin = $request->getHeader('origin');

    if (!(
      ($this->isDebugMode && $this->allowAllOrigins) ||
      (in_array('*', $this->allowedOrigins)) ||
      (count($origin) && in_array($origin[0], $this->allowedOrigins))
    )) {
      //forbidden request - origin not allowed
      return $response
        ->withStatus(ResponseCode::FORBIDDEN);
    } else {
      $this->errorModel->id = $this->errorId ?? self::getErrorId();
      if ($this->isDebugMode) {
        $this->errorModel->code = ResponseCode::errorCodes[$code]['name'];
        $this->errorModel->message = !empty($error->getMessage()) ? $error->getMessage(
        ) : ResponseCode::errorCodes[$code]['msg'];
        $this->errorModel->file = $error->getFile();
        $this->errorModel->line = $error->getLine();
      } else {
        $code = ResponseCode::ERROR;
        $this->errorModel->code = ResponseCode::errorCodes[$code]['name'];
        $this->errorModel->message = ResponseCode::errorCodes[$code]['msg'];
        $this->errorModel->file = null;
        $this->errorModel->line = null;
      }

      if (!count($origin)) {
        $origin = ['*'];
      }

      $response
        ->getBody()
        ->write(
          json_encode(
            $this->errorModel,
            JSON_UNESCAPED_UNICODE | ($this->isDebugMode ? JSON_PRETTY_PRINT : null
            )
          )
        );

      return $response
        ->withHeader('Content-Type', 'application/json')
        ->withHeader('Access-Control-Allow-Credentials', 'true')
        ->withHeader('Access-Control-Allow-Origin', $origin[0])
        ->withStatus($code);
    }
  }
}
