<?php

namespace CDN;

mb_internal_encoding('UTF-8');

use CDN\Classes\ErrorHandler;
use CDN\Classes\SwaggerNotationGenerator;
use CDN\Models\ErrorModel;
use DateInterval;
use DateTime;


$config = require __DIR__.'/../../env.php';
if (preg_match('/^.*\/(jp[e]?g|png|webp)/', $_SERVER["REQUEST_URI"], $matches)) {
  $ext = $matches[1];
} else {
  $ext = 'png';
}

$image = str_replace('//', '/', $config['static_folder'].strtoupper($_SERVER["REQUEST_URI"]).DIRECTORY_SEPARATOR.'image.'.$ext);

if (file_exists($image)) {
  $d = new DateTime('UTC');
  header('Cache-Control: public,max-age='.$config['expires']);
  header('Content-Disposition: inline; filename="'.str_replace('/', '_', trim($_SERVER["REQUEST_URI"], '/')).'.'.$ext.'"');
  header('Content-Type: image/'.$ext);
  header('Expires:'.$d->add(new DateInterval('PT'.$config['expires'].'S'))->format('D, d M Y H:i:s \G\M\T'));
  readfile($image);
  die();
}

require '../../vendor/autoload.php';


use DI\Bridge\Slim\App;
use DI\Container;
use DI\ContainerBuilder;
use Monolog\Logger as MLogger;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\StreamHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Wsdl2PhpGenerator\Config;
use Wsdl2PhpGenerator\Generator;

$app = new class() extends App
{
  protected function configureContainer(ContainerBuilder $builder)
  {
    $builder->addDefinitions(__DIR__.'/../../env.php');
    $exceptionCatcher = function (Container $c) {
      return new ErrorHandler($c, new ErrorModel());
    };
    $definitions = [
      'logger' => function (ContainerInterface $c) {
        $logger = new MLogger('cdn');
        $debugConfig = $c->get('debug_config');
        $d = new DateTime('UTC');
        $stream = new StreamHandler($debugConfig['app_log_path'] . 'app_' . $d->format('Ymd') . '.log', $debugConfig['debug_level']);
        $fingersCrossed = new FingersCrossedHandler($stream, $debugConfig['debug_level']);
        $logger->pushHandler($fingersCrossed);

        return $logger;
      },
      'missing_images_logger' => function (ContainerInterface $c) {
        $logger = new MLogger('missing_images');
        $debugConfig = $c->get('debug_config');
        $d = new DateTime('UTC');
        $stream = new StreamHandler($debugConfig['missing_images_log_path'] . 'missing_images_' . $d->format('Ymd') . '.log', $debugConfig['debug_level']);
        $fingersCrossed = new FingersCrossedHandler($stream, $debugConfig['debug_level']);
        $logger->pushHandler($fingersCrossed);

        return $logger;
      },
      'phpErrorHandler' => $exceptionCatcher,
      'errorHandler' => $exceptionCatcher,
    ];
    $builder->addDefinitions($definitions);
  }
};

$container = $app->getContainer();

$app->get(
  '/',
  function (Request $request, Response $response) use ($container) {
    $docs = file_get_contents('docs.html');
    echo str_replace('{{CDN_HOST}}', $container->get('cdn_host'), $docs);
  }
);

$app->get(
  '/info',
  function (Request $request, Response $response) use ($container) {
    phpinfo();
  }
);

$app->get(
  '/wsdl',
  function (Request $request, Response $response) use ($container) {
    //restart docker if this doesn't work
    $axWebservices = $container->get('ax_webservices');
    $swaggeredClasses = array();
    foreach ($axWebservices as $name => $axWebservice) {
      $path = $container->get('root_dir').'/src/app/Wsdl/'.$name.'/';
      $swaggerGenerator = new SwaggerNotationGenerator($path, false, $swaggeredClasses);
      $swaggerGenerator->removeDirectory();
      $generator = new Generator();
      $generator->generate(
        new Config(
          array(
            'inputFile' => $axWebservice['host'],
            'outputDir' => $path,
            'namespaceName' => 'CDN\\Wsdl\\'.$name,
            'soapClientOptions' => array_merge(
              array(
                'login' => $axWebservice['user'],
                'password' => $axWebservice['pass'],
              ),
              $axWebservice['options']
            ),
          )
        )
      );
      $swaggerGenerator->generate();
      $response
        ->getBody()
        ->write($swaggerGenerator->getLog());
      $swaggeredClasses = $swaggerGenerator->getGenerated();
    }

    return $response
      ->withHeader('Content-Type', 'text/html;charset=UTF-8');
  }
);

$app->get(
  '/get/{groupId}[/{force}]',
  ['\CDN\Controllers\ImageController', 'getModelImages']
);

$app->get(
  '/dbcache/mongo/update',
  ['\CDN\Controllers\CacheController', 'updateMongo']
);

$app->get(
  '/dbcache/mongo/update/forced',
  ['\CDN\Controllers\CacheController', 'forcedUpdateMongo']
);

$app->get(
  '/{model}[/{view}[/{dim1}[/{dim2}[/{dim3}[/{dim4}]]]]]',
  ['\CDN\Controllers\ImageController', 'getImage']
);

$app->run();