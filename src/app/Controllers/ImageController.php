<?php
/**
 * @file
 * Contains class ConfiguratorController code implementation.
 */

namespace CDN\Controllers;

use CDN\Classes\Helpers\DimType;
use CDN\Classes\Helpers\ProductGroupId;
use CDN\Classes\ProcessWindows;
use CDN\Classes\ProductCodeParser;
use CDN\Models\Dim;
use CDN\Models\Path;
use CDN\Models\Product;
use CDN\Wsdl\B2BHelperTER\B2BHelper_TERGetRadiatorModelsByProdGroupRequest;
use CDN\Wsdl\B2BHelperTER\NoYesAll_TER;
use CDN\Wsdl\B2BHelperTER\RoutingService;
use DateTime;
use DI\Container;
use finfo;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Process\Process;

class ImageController extends Controller
{
  protected $routingService;
  protected $productModel;
  protected $parser;
  protected $hash;
  protected $dims = array();
  protected $uri;

  const LOG = 'log';
  const SHORT_LOG = 'short';
  const LONG_LOG = 'long';

  const NO_RENDER_DIR = 'CAMX';
  const NO_COLOR_DIR = 'default';

  const DEF_CONN = 'SX';
  const DEF_EXT = 'png';

  const GROUP_DIRS = [
    ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS => 'Radiators',
    ProductGroupId::AX_PRODUCT_GROUP_IRON_RADIATORS => 'Radiators',
    ProductGroupId::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS => 'Radiators',
    ProductGroupId::AX_PRODUCT_GROUP_HEATERS => 'Heaters',
    ProductGroupId::AX_PRODUCT_GROUP_ACCESSORIES => 'Accessories',
    ProductGroupId::AX_PRODUCT_GROUP_COMMODITY => 'Accessories',
  ];

  const DIACR = [' ', 'ą', 'ż', 'ś', 'ź', 'ć', 'ń', 'ę', 'ó', 'ł', 'Ą', 'Ż', 'Ś', 'Ź', 'Ć', 'Ń', 'Ę', 'Ó', 'Ł'];
  const REPLS = ['_', 'a', 'z', 's', 'z', 'c', 'n', 'e', 'o', 'l', 'A', 'Z', 'S', 'Z', 'C', 'N', 'E', 'O', 'L'];

  const MIME_TYPES = [
    'png' => 'image/png',
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'webp' => 'image/webp',
  ];

  const VIEWS = [
    'CAM0',
    'CAM1',
    'CAM2',
    'CAM3',
    'CAM4',
    'CAM5',
  ];

  const IMAGE_TYPES = [
    'png',
    'jpg',
    'jpeg',
    'webp',
  ];

  /**
   * ImageController constructor.
   *
   * ImageController constructor.
   * @param Container $container
   * @param RoutingService $routingService
   * @param Product $productModel
   * @param ProductCodeParser $parser
   */
  public function __construct(
    Container $container,
    RoutingService $routingService,
    Product $productModel,
    ProductCodeParser $parser
  ) {
    $this->routingService = $routingService;
    $this->productModel = $productModel;
    $this->parser = $parser;
    $this->hash = bin2hex(random_bytes(3));
    parent::__construct($container);
  }

  /**
   * Get default images from remote site for products returned from AX
   *   force - force image downloading, get images even if exist locally
   *   ?log - show method log (short/long), query param
   *
   * @param string $groupId
   *   Product group; WYRG/WYRZ
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getModelImages(string $groupId, Request $request, Response $response): Response
  {
    if ($this->container->get('debug_config')['debug_mode'] ?? true) {
      $this->logger->addInfo('started getting default images');
    }
    $log = array_key_exists(self::LOG, $request->getQueryParams()) ? $request->getQueryParams()[self::LOG] : null;
    $force = array_key_exists('force', $request->getAttribute('route')->getArguments());
    $models = [];
    // TODO move 'language' to method parameters
    // get models from AX
    $params = new B2BHelper_TERGetRadiatorModelsByProdGroupRequest($groupId, NoYesAll_TER::Yes, 'pl', '');
    $axModels = $this->routingService->GetRadiatorModelsByProdGroup($params)->getResponse(
    )->getRadiatorModelB2BStruct_TER();
    $modelImages = [];
    // get images' paths from DB and prepare urls
    foreach ($this->productModel->getImages($groupId) as $image) {
      $modelImages[$image->modelAX] = $image->prepareImage();
    }
    foreach ($axModels as $axModel) {
      $modelShortName = substr($axModel->getRadiatorModel(), -3);
      if (array_key_exists($modelShortName, $modelImages)) {
        $model = array(
          'label' => $axModel->getLabelName(),
          'image' => $modelImages[$modelShortName],
        );
        $modelDir = str_replace(self::DIACR, self::REPLS, $model['label']);
        $path = new Path(
          $this->container->get('images_url') . self::GROUP_DIRS[$groupId] . DIRECTORY_SEPARATOR,
          $this->container->get('images_path') . self::GROUP_DIRS[$groupId] . DIRECTORY_SEPARATOR
        );
        $path->base .= $modelDir . DIRECTORY_SEPARATOR . self::NO_RENDER_DIR . DIRECTORY_SEPARATOR;
        $path->file = str_replace(self::DIACR, self::REPLS, $model['label']);
        $path->ext = $model['image']->ext;
        // get image if doesn't exist or forced
        if (!file_exists($path->getFullPath()) || $force) {
          $file = @fopen($modelImages[$modelShortName]->uri, 'r');
          if ($file === false) {
            if ($this->container->get('debug_config')['log_exceptions'] ?? true) {
              $this->logger->addError(json_encode(error_get_last()));
            }
          } else {
            if (!is_dir($path->base)) {
              if (!@mkdir($path->base, 0777, true)) {
                if ($this->container->get('debug_config')['log_exceptions'] ?? true) {
                  $this->logger->addError(json_encode(error_get_last()));
                }
              }
            }
            file_put_contents($path->getFullPath(), $file);
            $src = "--src=" . $path->getFullPath();
            $dest = "--dest=" . $path->base;
            $silent = $log ? "" : "--silent";
            $hash = "--hash=" . $this->hash;
            $cmd = implode(
              " ",
              [
                "node " . $this->container->get('root_dir') . "/src/app/Scripts/compress.js",
                $src,
                $dest,
                $silent,
                $hash,
              ]
            );
            $output = null;
            exec(
              $cmd,
              $output
            );
            if ($log === self::LONG_LOG) {
              $response
                ->getBody()
                ->write(
                  "<p><b>" . $axModel->getRadiatorModel() . " " . $axModel->getLabelName() . "</b></p>" .
                  "<p>compress log:</p>" .
                  "<pre>" .
                  json_encode($output, $this->isDebugMode ? JSON_PRETTY_PRINT : null) .
                  "</pre>"
                );
              $response = $this->logResponse($path, $request, $response);
            }
            $models[$axModel->getRadiatorModel()] = $model;
            if ($this->container->get('debug_config')['debug_mode'] ?? true) {
              $this->logger->addInfo('downloaded file: ' . $path->getFullPath());
            }
          }
        } elseif ($this->container->get('debug_config')['debug_mode'] ?? true) {
          $this->logger->addInfo('skipping file: ' . $path->getFullPath() . ' already exists');
        }
      }
    }
    if ($log === self::SHORT_LOG) {
      $response
        ->getBody()
        ->write(
          "<pre>" .
          json_encode($models, $this->isDebugMode ? JSON_PRETTY_PRINT : null) .
          "</pre>"
        );
    }

    if (!$log) {
      $msg = "<p>copied " . count($models) . " file" . (count($models) === 1 ? "" : "s") . "</p>";
      $cnt = 0;
      foreach ($models as $i => $model) {
        $msg .= ++$cnt . ":  " . $i . "; " . $model['image']->uri . "<br>";
      }
      $response
        ->getBody()
        ->write($msg);
    }
    if ($this->container->get('debug_config')['debug_mode'] ?? true) {
      $this->logger->addInfo('finished getting default images');
    }

    return $response;
  }

  /**
   * Get image based on url path passed to cdn
   *   model - product model, in path, required
   *   view - angle of a rendered image, in path, required
   *   dim1 - model size, in path
   *   dim2 - model color, in path
   *   dim3 - image width/format, in path
   *   dim4 - image width/format, in path
   *   ?log - show log instead of image, query param
   *
   * @param string $model
   *   Product model, required
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws \Exception
   */
  public function getImage(string $model, Request $request, Response $response): Response
  {
    //add 'log' query param to output log instead of image
    $showLog = array_key_exists(self::LOG, $request->getQueryParams());

    $this->uri = $request->getUri()->getPath();

    foreach ($request->getAttribute('route')->getArguments() as $name => $argument) {
      $argument = strtoupper($argument);
      if ($this->parser->isRadiator($argument)) {
        //model sku - radiator (WG, WL, WS, WW, WZ)
        $this->parser->parseRadiator($argument);
        $this->dims[DimType::RADIATOR] = new Dim(DimType::RADIATOR, $this->parser->getValues(), 'radiator sku');
      } elseif ($this->parser->isHeater($argument)) {
        //model sku - heater (WE)
        $this->parser->parseHeater($argument);
        $this->dims[DimType::HEATER] = new Dim(DimType::HEATER, $this->parser->getValues(), 'heater sku');
      } elseif ($this->parser->isAccessory($argument)) {
        //model sku = accessory (WR)
        $this->parser->parseAccessory($argument);
        $this->dims[DimType::ACCESSORY] = new Dim(DimType::ACCESSORY, $this->parser->getValues(), 'accessory sku');
      } elseif ($this->parser->isCommodity($argument)) {
        //model sku = commodity (TG)
        $this->parser->parseCommodity($argument);
        $this->dims[DimType::COMMODITY] = new Dim(DimType::COMMODITY, $this->parser->getValues(), 'accessory sku');
      } else {
        if ($name === 'model') {
          $this->dims[DimType::MODEL] = new Dim(
            DimType::MODEL, str_replace(self::DIACR, self::REPLS, $argument), $name
          );
        } else {
          if (in_array($argument, self::VIEWS)) {
            $this->dims[DimType::CAM_VIEW] = new Dim(DimType::CAM_VIEW, $argument, $name);
          } elseif ($argument === self::NO_RENDER_DIR) {
            $this->dims[DimType::DEF_VIEW] = new Dim(DimType::DEF_VIEW, $argument, $name);
          } elseif (preg_match('/[0-9]+_[0-9]+/', $argument)) {
            // model size
            $this->dims[DimType::MODEL_SIZE] = new Dim(DimType::MODEL_SIZE, $argument, $name);
          } elseif (preg_match('/(?!jp[e]?g|png|webp)\b(?:RAL_[0-9]{3,}|[A-Za-z])+/i', $argument)) {
            // model color
            $this->dims[DimType::MODEL_COLOR] = new Dim(DimType::MODEL_COLOR, $argument, $name);
          } elseif (preg_match('/^[0-9]+$/', $argument)) {
            // image size (width in pixels)
            $this->dims[DimType::IMAGE_WIDTH] = new Dim(DimType::IMAGE_WIDTH, $argument, $name);
          } elseif (in_array(strtolower($argument), self::IMAGE_TYPES)) {
            // image type (jpg/png/webp)
            $this->dims[DimType::IMAGE_TYPE] = new Dim(
              DimType::IMAGE_TYPE,
              strtolower($argument) === 'jpeg' ? 'jpg' : strtolower($argument),
              $name
            );
          }
        }
      }
    }

    if (array_key_exists(DimType::RADIATOR, $this->dims)) {
      $groupId = ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
      $this->dims[DimType::MODEL] = new Dim(DimType::MODEL, $this->parser->getFolder());
      if (!array_key_exists(DimType::CAM_VIEW, $this->dims)) {
        $this->dims[DimType::CAM_VIEW] = new Dim(DimType::CAM_VIEW, $this->container->get('def_view'));
      }
      if ($this->parser->getHeight() !== '' && $this->parser->getWidth() !== '') {
        $this->dims[DimType::MODEL_SIZE] = new Dim(
          DimType::MODEL_SIZE,
          $this->parser->getHeight() . '_' . $this->parser->getWidth()
        );
      }
      if ($this->parser->getColor() !== '') {
        $this->dims[DimType::MODEL_COLOR] = new Dim(
          DimType::MODEL_COLOR,
          str_replace(' ', '_', $this->parser->getColor())
        );
      }
    } elseif (array_key_exists(DimType::HEATER, $this->dims)) {
      $groupId = ProductGroupId::AX_PRODUCT_GROUP_HEATERS;
      $this->dims[DimType::MODEL] = new Dim(DimType::MODEL, $this->parser->getFolder());
      $this->dims[DimType::MODEL_COLOR] = new Dim(
        DimType::MODEL_COLOR, str_replace(' ', '_', $this->parser->getColor())
      );
      $this->dims[DimType::CAM_VIEW] = new Dim(DimType::CAM_VIEW, self::NO_RENDER_DIR);
    } elseif (array_key_exists(DimType::ACCESSORY, $this->dims)) {
      $groupId = ProductGroupId::AX_PRODUCT_GROUP_ACCESSORIES;
      $this->dims[DimType::MODEL] = new Dim(DimType::MODEL, $this->parser->getModel());
      if (!array_key_exists(DimType::CAM_VIEW, $this->dims)) {
        $this->dims[DimType::CAM_VIEW] = new Dim(
          DimType::CAM_VIEW,
          $this->parser->getSubgroupIdShort() === 'W' ? $this->container->get('def_view') : self::NO_RENDER_DIR
        );
      }
    } elseif (array_key_exists(DimType::COMMODITY, $this->dims)) {
      $groupId = ProductGroupId::AX_PRODUCT_GROUP_COMMODITY;
      $this->dims[DimType::MODEL] = new Dim(DimType::MODEL, $this->parser->getModel());
      if (!array_key_exists(DimType::CAM_VIEW, $this->dims)) {
        $this->dims[DimType::CAM_VIEW] = new Dim(DimType::CAM_VIEW, self::NO_RENDER_DIR);
      }
    } else {
      //TODO for now ok, because all radiators (WYRG/WYRL/WYRZ) are in the same folder
      foreach (self::GROUP_DIRS as $group => $dir) {
        $p = $this->container->get(
            'images_path'
          ) . $dir . DIRECTORY_SEPARATOR . $this->dims[DimType::MODEL]->value . DIRECTORY_SEPARATOR;
        if (is_dir($p) && file_exists($p)) {
          $groupId = $group;
          break;
        }
      }
    }

    $path = $this->buildPath($groupId);

    if ($showLog) {
      $response
        ->getBody()
        ->write($this->getLogMsg($path));
    }

    if (count(
        array_intersect_key(
          array_flip([DimType::RADIATOR, DimType::HEATER, DimType::ACCESSORY, DimType::COMMODITY]),
          $this->dims
        )
      ) && !$this->parser->isValid()) {
      //product code (SKU) is incorrect
      $response
        ->getBody()
        ->write("no image");

      return $response->withStatus(404);
    }

    if (!$path->isValid()) {
      //dims passed in url are incorrect
      $response
        ->getBody()
        ->write("wrong url construction");

      return $response->withStatus(404);
    }

    if (array_key_exists(DimType::MODEL_COLOR, $this->dims) && !$path->foundFiles()) {
      //no product with chosen color
      $this->missingImagesLogger->addError('request: ' . $request->getUri() . '; search path: ' . $path->getSrcPath());
      $response
        ->getBody()
        ->write("no image");

      return $response->withStatus(404);
    }

    //path parsed correctly
    if (file_exists($path->getFullPath())) {
      //file exists
      if ($showLog) {
        //with log
        $response = $this->logResponse($path, $request, $response);
      } else {
        //only image
        $response = $this->imageResponse($path, $request, $response);
      }
    } else {
      //create file
      //file type and/or resolution
      $output = $this->createFile($path, $showLog);

      if ($showLog) {
        //with log
        $response
          ->getBody()
          ->write(
            "<p>convert output:" . $output[0] . "</p>" .
            "<p>convert log:</p>" .
            "<pre>" .
            $output[1] .
            "</pre>"
          );
        $response = $this->logResponse($path, $request, $response);
      } else {
        //only image
        $response = $this->imageResponse($path, $request, $response);
      }
    }

    return $response;
  }

  protected function createFile(Path $path, bool $showLog)
  {
    $src = "--src=" . $path->getSrcPath();
    $dest = "--dest=" . $path->base;
    $format = "--format=" . $path->ext;
    $silent = $showLog ? "" : "--silent";
    $width = array_key_exists(
      DimType::IMAGE_WIDTH,
      $this->dims
    ) ? "--width=" . $this->dims[DimType::IMAGE_WIDTH]->value : '';
    $cmd = implode(
      " ",
      [
        "node " . $this->container->get('root_dir') . "/src/app/Scripts/convert.js",
        $src,
        $dest,
        $format,
        $width,
        $silent,
      ]
    );
    $process = new ProcessWindows($cmd, $this->logger, $this->container->get('debug_config')['proc_error_log_path']);
    $output = $process->runWindows();

    return [$output, $process->getContents()];
  }

  /**
   * Get log message
   *
   * @param Path $path
   *
   * @return string
   */
  protected function getLogMsg(Path $path): string
  {
    return
      "<p>path:</p>" .
      "<pre>" .
      "\nexists: " . (file_exists($path->getFullPath()) ? "yes" : "no") .
      "\nvalid: " . ($path->isValid() ? "yes" : "no") .
      "\nsearch pattern: " . $path->searchPath .
      "\nfound files: " . json_encode($path->foundFiles, JSON_PRETTY_PRINT) .
      "\nfull: " . $path->getFullPath() .
      "\nbase: " . $path->base .
      "\nfile: " . $path->file .
      "\next: " . $path->ext .
      "\nmime: " . $path->mimeType .
      "\nsaveAs: " . $path->nameForDownload .
      "\nsrcPath: " . $path->getSrcPath() .
      "\nsrcExt: " . $path->srcExt .
      "\nsrcBase: " . $path->srcBase .
      "</pre>" .
      "<p>dims:</p>" .
      "<pre>" .
      json_encode($this->dims, $this->isDebugMode ? JSON_PRETTY_PRINT : null) .
      "</pre>";
  }

  /**
   * Copy requested image to static folder (for faster retrieval on subsequent requests)
   *
   * @param Path $path
   * @param Request $request
   */
  protected function copyToStaticFolder(Path $path, Request $request)
  {
    $filepath = str_replace(
      '//',
      '/',
      $this->container->get('static_folder') . strtoupper(
        $request->getUri()->getPath()
      ) . DIRECTORY_SEPARATOR . 'image.' . $path->ext
    );
    Path::createDirectory($filepath);
    copy($path->getFullPath(), $filepath);
  }

  /**
   * Build image path from identified dimensions
   * @param string $groupId
   *
   * @return Path
   */
  protected function buildPath(string $groupId): Path
  {
    $path = new Path(
      $this->container->get('images_url') . self::GROUP_DIRS[$groupId] . DIRECTORY_SEPARATOR,
      $this->container->get('images_path') . self::GROUP_DIRS[$groupId] . DIRECTORY_SEPARATOR
    );
    if (array_key_exists(DimType::HEATER, $this->dims)) {
      $path->base .= implode(
          DIRECTORY_SEPARATOR,
          [
            $this->dims[DimType::HEATER]->value['folder'],
            $this->dims[DimType::CAM_VIEW]->value,
          ]
        ) . DIRECTORY_SEPARATOR;
      $path->file = strtolower(
        implode(
          '_',
          [
            $this->dims[DimType::HEATER]->value['folder'],
            str_replace(' ', '_', $this->dims[DimType::HEATER]->value['color']),
          ]
        )
      );
      $path->ext = self::DEF_EXT;
      $path->mimeType = self::MIME_TYPES[$path->ext];
      $path->searchFiles();
      $path->srcBase = $path->base;
      $path->srcExt = $path->ext;
    } elseif (array_key_exists(DimType::ACCESSORY, $this->dims)) {
      $path->base .= implode(
          DIRECTORY_SEPARATOR,
          [
            $this->dims[DimType::ACCESSORY]->value['folder'],
            $this->dims[DimType::CAM_VIEW]->value,
          ]
        ) . DIRECTORY_SEPARATOR;
      $path->file = $this->dims[DimType::ACCESSORY]->value['code'];
      $path->ext = self::DEF_EXT;
      $path->mimeType = self::MIME_TYPES[$path->ext];
      $path->searchFiles();
      $path->srcBase = $path->base;
      $path->srcExt = $path->ext;
    } elseif (array_key_exists(DimType::COMMODITY, $this->dims)) {
      $path->base .= implode(
          DIRECTORY_SEPARATOR,
          [
            $this->dims[DimType::COMMODITY]->value['folder'],
            $this->dims[DimType::CAM_VIEW]->value,
          ]
        ) . DIRECTORY_SEPARATOR;
      $path->file = $this->dims[DimType::COMMODITY]->value['code'];
      $path->ext = self::DEF_EXT;
      $path->mimeType = self::MIME_TYPES[$path->ext];
      $path->searchFiles();
      $path->srcBase = $path->base;
      $path->srcExt = $path->ext;
    } elseif (array_key_exists(DimType::MODEL, $this->dims)) {
      $path->base .= $this->dims[DimType::MODEL]->value . DIRECTORY_SEPARATOR;
      if (array_key_exists(DimType::CAM_VIEW, $this->dims)) {
        $tmpDir = $path->base . $this->dims[DimType::CAM_VIEW]->value . DIRECTORY_SEPARATOR;
        if (is_dir($tmpDir) && file_exists($tmpDir)) {
          $path->base .= $tmpDir;
          if (array_key_exists(DimType::MODEL_SIZE, $this->dims)) {
            if (array_key_exists(DimType::MODEL_COLOR, $this->dims)) {
              $path->file = implode(
                '_',
                [
                  $this->dims[DimType::MODEL]->value,
                  $this->dims[DimType::MODEL_SIZE]->value,
                  $this->dims[DimType::MODEL_COLOR]->value,
                  '*',
                ]
              );
              $path->ext = self::DEF_EXT;
              $path->mimeType = self::MIME_TYPES[$path->ext];
            } else {
              $path->base .= self::NO_COLOR_DIR . DIRECTORY_SEPARATOR;
              $path->file = implode(
                '_',
                [
                  $this->dims[DimType::MODEL]->value,
                  $this->dims[DimType::MODEL_SIZE]->value,
                ]
              );
              $path->ext = self::DEF_EXT;
              $path->mimeType = self::MIME_TYPES[$path->ext];
            }
          } else {
            $path->base .= self::NO_COLOR_DIR . DIRECTORY_SEPARATOR;
            $this->scanForFiles($path);
          }
        } else {
          $path->base .= self::NO_RENDER_DIR . DIRECTORY_SEPARATOR;
          $this->scanForFiles($path, true);
        }
      } else {
        $path->base .= self::NO_RENDER_DIR . DIRECTORY_SEPARATOR;
        $this->scanForFiles($path, true);
      }

      $path->searchFiles();
      $path->srcBase = $path->base;
      $path->srcExt = $path->ext;
    }

    if ($path->isValid()) {
      if (array_key_exists(DimType::IMAGE_TYPE, $this->dims)) {
        $path->ext = $this->dims[DimType::IMAGE_TYPE]->value;
        $path->mimeType = self::MIME_TYPES[$path->ext];
      }
      if (array_key_exists(DimType::IMAGE_WIDTH, $this->dims)) {
        $path->base .= $this->dims[DimType::IMAGE_WIDTH]->value . 'w/';
      }
    }

    $path->nameForDownload =
      str_replace('/', '_', strtoupper(trim($this->uri, DIRECTORY_SEPARATOR))) . '.' . $path->ext;

    return $path;
  }

  protected function scanForFiles(Path $path, bool $byFinfo = false)
  {
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $files = @scandir($path->base);
    if ($files) {
      foreach ($files as $f) {
        $fArr = explode('.', $f);
        if (count($fArr) === 2 && $fArr[1] === self::DEF_EXT) {
          // TODO what if filename has dots in it?
          $path->file = $fArr[0];
          $path->ext = $fArr[1];
          $path->mimeType = $byFinfo ? $finfo->file($path->getFullPath()) : self::MIME_TYPES[$path->ext];;
          break;
        }
      }
    }
  }

  /**
   * Create response with image
   *
   * @param Path $path
   *   Path object
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws \Exception
   */
  protected function imageResponse(Path $path, Request $request, Response $response): Response
  {
    $image = @file_get_contents($path->getFullPath());
    if ($image !== false) {
      $this->copyToStaticFolder($path, $request);

      $response
        ->getBody()
        ->write($image);

      $exp = $this->container->get('expires');
      $d = new DateTime('UTC');
      $d->add(new \DateInterval('PT' . $exp . 'S'));

      return $response
        ->withHeader('Cache-Control', 'public,max-age=' . $exp)
        ->withHeader('Content-Disposition', 'inline; filename="' . $path->nameForDownload . '"')
        ->withHeader('Content-Type', $path->mimeType)
        ->withHeader('Expires', $d->format('D, d M Y H:i:s \G\M\T'));
    } else {
      $this->missingImagesLogger->addError('request: ' . $request->getUri() . '; search path: ' . $path->getSrcPath());
      $response
        ->getBody()
        ->write("no image");

      return $response;
    }
  }

  /**
   * Create response with log
   *
   * @param Path $path
   *   Path object
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  protected function logResponse(Path $path, Request $request, Response $response): Response
  {
    $image = @file_get_contents($path->getFullPath());
    if (!$image) {
      $response
        ->getBody()
        ->write(
          "<p style='color:red'>if generated webp file is bigger tham png then script saves file as png</p>" .
          "<pre>" .
          json_encode(error_get_last(), $this->isDebugMode ? JSON_PRETTY_PRINT : null) .
          "</pre>"
        );
    } else {
      $fsize = filesize($path->getFullPath()) / 1024;
      $isize = getimagesize($path->getFullPath());
      $response
        ->getBody()
        ->write(
          "<p>image:</p>" .
          "<pre>" .
          "size: " . str_replace(".", ",", strval(round($fsize, 2))) . " KB" .
          "\n" . $isize[3] .
          "\nmime: " . $isize['mime'] .
          "</pre>" .
          "<img style='border:2px solid black;max-width:100%' src='" . $path->getUrl() . "'>" .
          "<br><br>"
        );
    }

    return $response;
  }
}
