<?php

namespace CDN\Controllers;

use DI\Container;

class Controller
{
  protected $total;
  protected $logger;
  protected $missingImagesLogger;
  protected $container;
  protected $isDebugMode;

  public function __construct(Container $container)
  {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->missingImagesLogger = $container->get('missing_images_logger');
    $this->isDebugMode = $container->get('debug_config')['debug_mode'] ?? false;
  }
}
