<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 18.01.2018
 * Time: 07:52
 */

namespace CDN\Models;

// TODO move to classes - not dependent on db
class Path
{
  public $base;
  public $file;
  public $ext;
  public $mimeType;
  public $nameForDownload;
  public $srcBase;
  public $srcExt;

  public $searchPath;
  public $foundFiles;

  protected $host;
  protected $root;

  public function __construct($host, $base = null, $file = null, $ext = null)
  {
    $this->host = $host;
    $this->base = $base;
    $this->root = $base;
    $this->file = $file;
    $this->ext = $ext;
  }

  public function getFullPath()
  {
    return $this->base.$this->file.'.'.$this->ext;
  }

  public function getSrcPath()
  {
    return $this->srcBase.$this->file.'.'.$this->srcExt;
  }

  public function getUrl()
  {
    $p = str_replace($this->root, '', $this->getFullPath());

    return $this->host.$p;
  }

  public function searchFiles()
  {
    $this->searchPath = $this->getFullPath();
    $this->foundFiles = glob($this->searchPath);
    if (count($this->foundFiles)) {
      $foundFilesArr = explode(DIRECTORY_SEPARATOR, $this->foundFiles[0]);
      $this->file = explode('.', $foundFilesArr[count($foundFilesArr) - 1])[0];
    }
  }

  public function foundFiles(): bool
  {
    return count($this->foundFiles) ? true : false;
  }

  public function isValid()
  {
    if (
      empty($this->base) ||
      empty($this->file) ||
      empty($this->ext) ||
      empty($this->mimeType)
    ) {
      return false;
    }

    return true;
  }


  /**
   * Remove directory recursively including files & subfolders
   *
   * @param $src
   */
  public static function removeDirectory($src)
  {
    if (is_dir($src)) {
      $dir = opendir($src);
      while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
          $full = $src.DIRECTORY_SEPARATOR.$file;
          if (is_dir($full)) {
            self::removeDirectory($full);
          } else {
            unlink($full);
          }
        }
      }
      closedir($dir);
      rmdir($src);
    }
  }

  /**
   * Create directory for given filepath
   *
   * @param $filepath
   */
  public static function createDirectory(string $filepath)
  {
    $path = pathinfo($filepath);
    if (!file_exists($path['dirname'])) {
      mkdir($path['dirname'], 0777, true);
    }
  }
}