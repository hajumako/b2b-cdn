<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.03.2018
 * Time: 11:35
 */

namespace CDN\Models;


use CDN\Classes\Helpers\MongoDb;
use CDN\Classes\ProductCodeParser;
use CDN\Wsdl\B2BHelperTER\B2BHelper_TERGetAllRadiatorColorsRequest;
use CDN\Wsdl\B2BHelperTER\RadiatorColorsStruct_TER;
use DateTime;
use DI\Container;
use MongoDB\InsertOneResult;
use MongoDB\UpdateResult;
use stdClass;
use CDN\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\ArrayOfDTItemInfoReference;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\DTIntegrationItemInfoServiceGetProductDataRequest;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\DTItemInfoDataExt;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\DTItemInfoReference;
use CDN\Wsdl\DTIntegrationItemInfoServiceGroup\RoutingService AS DTRoutingService;

class Cache extends Model
{
  protected $logger;
  protected $isDebugMode;
  protected $B2BRoutingService;
  protected $DTRoutingService;

  const LAST_UPDATE = 'last_update';

  /**
   * Cache constructor.
   * @param Container $container
   * @param B2BRoutingService $B2BRoutingService
   * @param DTRoutingService $DTRoutingService
   */
  public function __construct(
    Container $container,
    B2BRoutingService $B2BRoutingService,
    DTRoutingService $DTRoutingService
  ) {
    $this->logger = $container->get('logger');
    $this->B2BRoutingService = $B2BRoutingService;
    $this->DTRoutingService = $DTRoutingService;
    $this->isDebugMode = $container->get('debug_config')['debug_mode'] ?? false;
    parent::__construct($container);
  }

  /**
   * @return stdClass
   */
  public function getLastUpdateDate(): stdClass
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_LAST_UPDATE,
      [
        'name' => self::LAST_UPDATE,
      ]
    );
    if (!$document) {
      return (object)array(
        'name' => self::LAST_UPDATE,
        'lastUpdate' => null,
      );
    }

    return $document->bsonSerialize();
  }

  /**
   * @param DateTime $dateTime
   * @param int $updatedRecords
   */
  public function setLastUpdateDate(DateTime $dateTime, int $updatedRecords)
  {
    $now = new DateTime();
    $this->upsertCollectionDoc(
      MongoDb::COLLECTION_LAST_UPDATE,
      [
        'name' => self::LAST_UPDATE,
      ],
      [
        'lastUpdate' => $dateTime->getTimestamp(),
        'timeOfCheck' => $now->getTimestamp(),
        'updatedRecords' => $updatedRecords,
      ]
    );
  }

  /**
   * @param $cacheInfo
   *
   * @return InsertOneResult
   */
  public function saveAXCacheInfo($cacheInfo): InsertOneResult
  {
    return $this->setCollectionDoc(
      MongoDb::COLLECTION_CACHE_INFO_LOG,
      $cacheInfo
    );
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getAccessorySpecs(string $itemId, string $configId): stdClass
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_ACCESSORY_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ]
    );

    return !$document ? $this->getAccessorySpecsFromAX($itemId, $configId) : $document->bsonSerialize();
  }

  /**
   * @param string $itemId
   * @param string|null $configId
   *
   * @return stdClass
   */
  public function getCommoditySpecs(string $itemId, string $configId = null): stdClass
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_COMMODITY_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ]
    );

    return !$document ? $this->getCommoditySpecsFromAX($itemId, $configId) : $document->bsonSerialize();
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getHeaterSpecs(string $itemId, string $configId): stdClass
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_HEATER_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ]
    );

    return !$document ? $this->getHeaterSpecsFromAX($itemId, $configId) : $document->bsonSerialize();
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getRadiatorSpecs(string $itemId, string $configId): stdClass
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_RADIATOR_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ]
    );

    return !$document ? $this->getRadiatorSpecsFromAX($itemId, $configId) : $document->bsonSerialize();
  }

  /**
   * @param string $colorId
   *
   * @return stdClass
   */
  public function getColor(string $colorId)
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_RADIATOR_COLORS,
      ['colorId' => $colorId]
    );

    return !$document ? $this->getColorFromAX($colorId) : $document->bsonSerialize();
  }

  public function getConfig(string $itemId, string $configId)
  {
    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_RADIATOR_CONFIGS,
      ['sku' => $itemId . '-' . $configId]
    );

    return !$document ? $this->getConfig($itemId, $configId) : $document->bsonSerialize();
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getAccessorySpecsFromAX(string $itemId, string $configId): stdClass
  {
    $specs = new stdClass();
    $axSpecs = $this->getProductDataFromAX($itemId, $configId);
    if ($axSpecs->getSpecifications()->getDTItemSpecification()) {
      foreach ($axSpecs->getSpecifications()->getDTItemSpecification() as $axSpec) {
        $specs->fullSpecs[] = [
          'name' => $axSpec->getPropatyName(),
          'value' => $axSpec->getPropertyValue(),
        ];
        if ($axSpec->getPropatyName() === 'Nazwa na etykietę') {
          $specs->label = str_replace('\'', '', $axSpec->getPropertyValue());
        }
      }
    }
    $specs->label = $specs->label ?: $itemId . '-' . $configId;
    if (substr($itemId, 2, 1) === 'W') {
      $specs->folder = substr($itemId, 0, 6);
    } else {
      $specs->folder = $this->container->get('others_folder');
    }
    $result = $this->upsertAccessoryData($itemId, $configId, $specs);
    $this->saveToLog($result);

    return $specs;
  }

  /**
   * @param string $itemId
   * @param string|null $configId
   *
   * @return stdClass
   */
  public function getCommoditySpecsFromAX(string $itemId, string $configId = null): stdClass
  {
    $specs = new stdClass();
    $axSpecs = $this->getProductDataFromAX($itemId, $configId);
    if ($axSpecs->getSpecifications()->getDTItemSpecification()) {
      foreach ($axSpecs->getSpecifications()->getDTItemSpecification() as $axSpec) {
        $specs->fullSpecs[] = [
          'name' => $axSpec->getPropatyName(),
          'value' => $axSpec->getPropertyValue(),
        ];
        if ($axSpec->getPropatyName() === 'Nazwa na etykietę') {
          $specs->label = str_replace('\'', '', $axSpec->getPropertyValue());
        }
      }
    }
    $specs->label = $specs->label ?: $itemId . '-' . $configId;
    $specs->folder = $this->container->get('others_folder');
    $result = $this->upsertCommodityData($itemId, $configId, $specs);
    $this->saveToLog($result);

    return $specs;
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getHeaterSpecsFromAX(string $itemId, string $configId): stdClass
  {
    $specs = new stdClass();
    $axSpecs = $this->getProductDataFromAX($itemId, $configId);
    if ($axSpecs->getSpecifications()->getDTItemSpecification()) {
      foreach ($axSpecs->getSpecifications()->getDTItemSpecification() as $axSpec) {
        $specs->fullSpecs[] = [
          'name' => $axSpec->getPropatyName(),
          'value' => $axSpec->getPropertyValue(),
        ];
      }
    }
    $specs->matches = ProductCodeParser::extractHeaterName($axSpecs->getProductName());
    $specs->label = $specs->matches[2] ?: $specs->matches[4];
    $specs->power = $specs->matches[5];
    $specs->folder = ProductCodeParser::normalizeHeaterName($specs->label);
    $result = $this->upsertHeaterData($itemId, $configId, $specs);
    $this->saveToLog($result);

    return $specs;
  }

  /**
   * @param string $itemId
   * @param string $configId
   *
   * @return stdClass
   */
  public function getRadiatorSpecsFromAX(string $itemId, string $configId): stdClass
  {
    $specs = new stdClass();
    $axSpecs = $this->getProductDataFromAX($itemId, $configId);
    if ($axSpecs->getSpecifications()->getDTItemSpecification()) {
      foreach ($axSpecs->getSpecifications()->getDTItemSpecification() as $axSpec) {
        $n = $axSpec->getPropatyName();
        $specs->fullSpecs[] = [
          'name' => $axSpec->getPropatyName(),
          'value' => $axSpec->getPropertyValue(),
        ];
        if ($n === 'Wysokość A [mm]') {
          $specs->height = $axSpec->getPropertyValue();
        } elseif ($n === 'Szerokość B [mm]') {
          $specs->width = $axSpec->getPropertyValue();
        } elseif ($n === 'Nazwa na etykietę') {
          $specs->label = str_replace('\'', '', $axSpec->getPropertyValue());
          $specs->folder = str_replace(' ', '_', $specs->label);
        }
      }
      //placed inside 'if' on purpose - do not save data to DB if product data not found
      $result = $this->upsertRadiatorData($itemId, $configId, $specs);
      $this->saveToLog($result);
    }

    return $specs;
  }

  /**
   * @param string $colorId
   *
   * @return stdClass
   */
  public function getColorFromAX(string $colorId = ''): stdClass
  {
    $color = new stdClass();
    $params = new B2BHelper_TERGetAllRadiatorColorsRequest();
    $axColors = $this->B2BRoutingService->GetAllRadiatorColors($params)->getResponse()->getRadiatorColorsStruct_TER();
    foreach ($axColors as $axColor) {
      if ($colorId === $axColor->getColorId()) {
        $color->colorId = strtoupper($axColor->getColorId());
        $color->colorName = $axColor->getColorName();
        $color->axColor = $axColor;
      }
      $result = $this->upsertColorData($axColor);
      $this->saveToLog($result);
    }

    return $color;
  }

  public function getConfigFromAX(string $itemId, string $configId)
  {

  }

  /**
   * @param string $itemId
   * @param string $configId
   * @param stdClass $specs
   *
   * @return UpdateResult
   */
  protected function upsertAccessoryData(string $itemId, string $configId, stdClass $specs): UpdateResult
  {
    return $this->upsertCollectionDoc(
      MongoDB::COLLECTION_ACCESSORY_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ],
      [
        'label' => $specs->label,
        'folder' => $specs->folder,
        'fullSpecs' => $specs->fullSpecs,
      ]
    );
  }

  /**
   * @param string $itemId
   * @param string|null $configId
   * @param stdClass $specs
   *
   * @return UpdateResult
   */
  protected function upsertCommodityData(string $itemId, string $configId = null, stdClass $specs): UpdateResult
  {
    return $this->upsertCollectionDoc(
      MongoDB::COLLECTION_COMMODITY_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ],
      [
        'label' => $specs->label,
        'folder' => $specs->folder,
        'fullSpecs' => $specs->fullSpecs,
      ]
    );
  }

  /**
   * @param string $itemId
   * @param string $configId
   * @param stdClass $specs
   *
   * @return UpdateResult
   */
  protected function upsertHeaterData(string $itemId, string $configId, stdClass $specs): UpdateResult
  {
    return $this->upsertCollectionDoc(
      MongoDB::COLLECTION_HEATER_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ],
      [
        'label' => $specs->label,
        'folder' => $specs->folder,
        'matches' => $specs->matches,
        'fullSpecs' => $specs->fullSpecs,
      ]
    );
  }

  /**
   * @param string $itemId
   * @param string $configId
   * @param stdClass $specs
   *
   * @return UpdateResult
   */
  protected function upsertRadiatorData(string $itemId, string $configId, stdClass $specs): UpdateResult
  {
    return $this->upsertCollectionDoc(
      MongoDB::COLLECTION_RADIATOR_SPECS,
      [
        'itemId' => $itemId,
        'configId' => $configId,
      ],
      [
        'height' => $specs->height,
        'width' => $specs->width,
        'label' => $specs->label,
        'folder' => $specs->folder,
        'fullSpecs' => $specs->fullSpecs,
      ]
    );
  }

  /**
   * @param RadiatorColorsStruct_TER $axColor
   *
   * @return UpdateResult
   */
  protected function upsertColorData(RadiatorColorsStruct_TER $axColor): UpdateResult
  {
    return $this->upsertCollectionDoc(
      MongoDb::COLLECTION_RADIATOR_COLORS,
      ['colorId' => strtoupper($axColor->getColorId())],
      [
        'colorName' => $axColor->getColorName(),
        'axColor' => $axColor->jsonSerialize(),
      ]
    );
  }

  /**
   * @param string $itemId
   * @param string|null $configId
   *
   * @return DTItemInfoDataExt
   */
  protected function getProductDataFromAX(string $itemId, string $configId = null): DTItemInfoDataExt
  {
    $item = new DTItemInfoReference();
    $item->setProductId($itemId);
    $item->setVariant($configId);
    $items = new ArrayOfDTItemInfoReference();
    $items->setDTItemInfoReference(array($item));
    $params = new DTIntegrationItemInfoServiceGetProductDataRequest($items);

    return $this->DTRoutingService->GetProductData($params)->getResponse()->getDTItemInfoDataExt()[0];
  }

  /**
   * @param UpdateResult $result
   */
  protected function saveToLog(UpdateResult $result)
  {
    if ($this->isDebugMode) {
      $this->logger->addInfo(
        'Updated radiator data | ' .
        'ack: ' . $result->isAcknowledged() .
        ($result->isAcknowledged() ?
          '; upsertedId: ' . $result->getUpsertedId() . '; modified: ' . $result->getModifiedCount(
          ) . '; upserted: ' . $result->getUpsertedCount()
          : ''
        )
      );
    }
  }
}