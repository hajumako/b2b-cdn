<?php

namespace CDN\Classes\Helpers;


abstract class DrupalProductBundle
{

  /**
   * Drupal constants - bundles of product nodes.
   */
  const HEATER = 'grzalka';
  const RADIATOR = 'grzejnik';
  const HEATING_APPLIANCE = 'akcesorium_grzewcze';
}
