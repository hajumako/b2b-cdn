<?php

namespace CDN\Classes\Helpers;

/**
 * @SWG\Definition(
 *   definition="ProductGroupIdShort",
 *   type="string",
 *   enum={"WG","WZ","WL","WE","WR","TG"},
 *   default="WYRG"
 * )
 */
abstract class ProductGroupIdShort
{
  /**
   * AX constants - short names of product groups.
   */
  const AX_PRODUCT_GROUP_NORMAL_RADIATORS = 'WG';
  const AX_PRODUCT_GROUP_IRON_RADIATORS = 'WZ';
  const AX_PRODUCT_GROUP_ELECTRIC_RADIATORS = 'WL';
  const AX_PRODUCT_GROUP_HEATERS = 'WE';
  const AX_PRODUCT_GROUP_ACCESSORIES = 'WR';
  const AX_PRODUCT_GROUP_COMMODITY = 'TG';

  /**
   * Get product group id by its long name
   *
   * @param string $groupId
   *   long name of product group id (ProductGroupId)
   * @return string
   */
  public static function getGroupId(string $groupId): string
  {
    switch ($groupId) {
      case ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_IRON_RADIATORS:
        return self::AX_PRODUCT_GROUP_IRON_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS:
        return self::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_HEATERS:
        return self::AX_PRODUCT_GROUP_HEATERS;
      case ProductGroupId::AX_PRODUCT_GROUP_ACCESSORIES:
        return self::AX_PRODUCT_GROUP_ACCESSORIES;
      case ProductGroupId::AX_PRODUCT_GROUP_COMMODITY:
        return self::AX_PRODUCT_GROUP_COMMODITY;
      default:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
    }
  }
}
