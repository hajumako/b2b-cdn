<?php

namespace CDN\Classes;

class SwaggerNotationGenerator
{
  const OK = 1;
  const ERR = 0;

  const DEV_SUFFIX = '_dev';

  private $isDev;
  private $log = '';

  private $wsdlClassesPath;
  private $classes;
  private $generated = [];

  private $patterns = [
    '/(namespace (?:[A-Za-z0-9\\\]+);)/',
    '/(class (?!ArrayOf)(\w+)(?: extends)*\s*\w*\s+{)(?!const )(\s+[\\/\\*]+)/',
    '/(class ArrayOf(?:\w+) [A-Za-z \\\,]+\s+{)/',
    '/(\\/\\*\\*)(?:\s+)(?:(?!(?:@var|@SWG\\\Definition))[\s\S])*?([ ]+)(\\* @var (?:string|boolean|int|float) (?:\\$[A-Za-z_]+)\s+[ ]+\\*\\/)/',
    '/(\\/\\*\\*)(?:\s+)(?:(?!(?:@var|@SWG\\\Definition))[\s\S])*?([ ]+)(\\* @var ArrayOf((?!string|boolean|int|float)\w+) (?:\\$[A-Za-z_]+)\s+[ ]+\\*\\/)/',
    '/(\\/\\*\\*)(?:\s+)(?:(?!(?:@var|@SWG\\\Definition))[\s\S])*?([ ]+)(\\* @var ArrayOf(string|boolean|int|float) (?:\\$[A-Za-z_]+)\s+[ ]+\\*\\/)/',
    '/(\\/\\*\\*)(?:\s+)(?:(?!(?:@var|@SWG\\\Definition))[\s\S])*?([ ]+)(\\* @var ((?!string|boolean|int|float|ArrayOf)\w+) (?:\\$[A-Za-z_]+)\s+[ ]+\\*\\/)/',
  ];

  private $nested = [
    false,
    false,
    false,
    false,
    true,
    false,
    true,
  ];

  private $check = [
    true,
    true,
    true,
    false,
    false,
    false,
    false,
  ];

  private $search = [
    '/(use CDN\\\Classes\\\Traits\\\JsonSerializer;)/',
    '/(\\/\\*\\* @SWG\\\Definition)/',
    '/(use JsonSerializer;)/',
  ];

  private $replacements = [
    // use statement for trait
    '${1}

use CDN\\Classes\\Traits\\JsonSerializer;'
    ,
    // Definition field before class & use trait
    '/** @SWG\Definition(
 *  definition="${2}",
 *  type="object"
 * )
 */
${1}
    use JsonSerializer;
    ${3}'
    ,
    // Add serializer to Array class
    '${1}
    use JsonSerializer;'
    ,
    // Property field for all strings & booleans
    '${1}
     * @SWG\\Property()
     ${3}'
    ,
    // Property field for arrays of objects
    '${1}
     * @SWG\\Property(
     *   type="array",
     *   @SWG\Items(ref="#/definitions/${4}")
     * )
     ${3}'
    ,
    // Property field for arrays of primitves
    '${1}
     * @SWG\\Property(
     *   type="array",
     *   @SWG\Items(type="${4}")
     * )
     ${3}'
    ,
    // Property field for objects
    '${1}
     * @SWG\\Property(
     *   ref="#/definitions/${4}"
     * )
     ${3}',
  ];

  /**
   * SwaggerNotationGenerator constructor.
   * @param string $wsdlClassesPath path to generated wsdl php classes
   * @param bool $isDev if swaggered files will be saved to separate files
   * @param array $generated
   */
  public function __construct(string $wsdlClassesPath, bool $isDev = false, array $generated = array())
  {
    $this->generated = $generated;
    $this->isDev = $isDev;
    $this->wsdlClassesPath = $wsdlClassesPath;
  }

  /**
   * Generate swagger-php notation for given files
   * https://github.com/zircote/swagger-php
   *
   * @param array $classes list of wsdl generated classes to get swaggered
   */
  public function generate(array $classes = null)
  {
    if ($classes == null) {
      if (is_dir($this->wsdlClassesPath)) {
        $files = scandir($this->wsdlClassesPath);
        if ($files) {
          $this->classes = $files;
        }
      }
    } else {
      $this->classes = $classes;
    }
    if ($this->classes) {
      foreach ($this->classes as $class) {
        $c = str_replace('.php', '', $class);
        if ($c != '.' && $c != '..') {
          $this->generateSwaggerFields($c, 0);
        }
      }
    }
  }

  /**
   * Get swagger generation log
   *
   * @return string log string
   */
  public function getLog(): string
  {
    return $this->log;
  }

  /**
   * Get list of generated swagger objects
   *
   * @return array
   */
  public function getGenerated(): array
  {
    return $this->generated;
  }

  /**
   * Remove directory recursively including files & subfolders
   *
   * @param null $path
   * @internal param $src
   */
  public function removeDirectory($path = null)
  {
    $src = $path ? $path : $this->wsdlClassesPath;
    if (is_dir($src)) {
      $dir = opendir($src);
      while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
          $full = $src.'/'.$file;
          if (is_dir($full)) {
            $this->removeDirectory($full);
          } else {
            unlink($full);
          }
        }
      }
      closedir($dir);
      rmdir($src);
    }
  }

  /** Recursive function for fields generation
   *
   * @param string $_class class (file) name
   * @param int $lvl recursion level
   */
  private function generateSwaggerFields(string $_class, int $lvl)
  {
    //check if already swaggered
    if (!in_array($_class, $this->generated)) {
      array_push($this->generated, $_class);
      $this->addToLog('swaggering '.$_class, $lvl);
      $path = $this->wsdlClassesPath.$_class.'.php';

      $contents = @file_get_contents($path);
      if ($contents) {
        $nested = [];
        foreach ($this->patterns as $i => $pattern) {
          if ($this->nested[$i]) {
            preg_match_all($pattern, $contents, $matches);
            $nested = array_merge($nested, $matches[4]);
          }
          //check if file already has a definition header
          if (
            !$this->check[$i] ||
            ($this->check[$i] && !preg_match($this->search[$i], $contents))
          ) {
            $contents = preg_replace($pattern, $this->replacements[$i], $contents, -1, $count);
            $last_err = preg_last_error();
            if ($last_err) {
              $this->addToLog('preg_replace error: '.$last_err.'; set php.ini pcre.jit=0', $lvl, self::ERR);
            }
          } else {
            $this->addToLog($_class.' found '.$this->search[$i], $lvl);
          }
        }
        //search fo const fields which indicate definition type of enum
        if (preg_match_all('/const ([A-Za-z_]+) = \'(\w+)\'/', $contents, $matches)) {
          $default = '';
          $enum = [];
          foreach ($matches[1] as $i => $match) {
            if ($match === '__default') {
              $default = '"'.$matches[2][$i].'"';
            } else {
              array_push($enum, '"'.$matches[2][$i].'"');
            }
          }

          $contents .= '
/**
 * @SWG\Definition(
 *   definition="'.$_class.'",
 *   type="string",
 *   enum={'.implode(',', $enum).'},
 *   default='.$default.'
 * )
 */';
        }

        //save file contents
        $outPath = $this->isDev ? ($this->wsdlClassesPath.$_class.self::DEV_SUFFIX.'.php') : $path;
        if (!file_exists($outPath)) {
          file_put_contents($outPath, '');
        }
        file_put_contents($outPath, $contents);

        //swagger nested classes
        if (count($nested)) {
          $this->addToLog('swaggering nested of '.$_class, $lvl);
          foreach ($nested as $i => $class) {
            /*if (!in_array($class, $this->generated)) {
              array_push($this->generated, $class);*/
            $this->generateSwaggerFields($class, $lvl + 1);
            /*} else {
              $this->addToLog($class . ' already swaggered', $lvl + 1);
            }*/
          }
        } else {
          $this->addToLog('no nested in '.$_class, $lvl);
        }
        $this->addToLog('swaggered '.$_class, $lvl);
      } else {
        $this->addToLog('file '.$path.' does not exist', $lvl, self::ERR);
      }
    } else {
      $this->addToLog($_class.' already swaggered', $lvl);
    }
  }

  /**
   * Add message to log
   * @param string $msg message
   * @param int $lvl recursion level
   * @param int $type message type
   */
  private function addToLog(string $msg, int $lvl = 0, int $type = self::OK)
  {
    $this->log .= '<p style="margin:0 0 .2em '.$lvl.'em'.($type === self::ERR ? ' color:red' : '').'">'.$msg.'</p>';
  }
}
