<?php

namespace CDN\Classes\Helpers;


use PDO;

class MariaDb
{
  private static $instance = null;

  private function __construct()
  {
  }

  private function __clone()
  {
  }

  final public static function getConnection(array $db)
  {
    if (!self::$instance) {
      self::$instance = new PDO(
        "mysql:host=".$db['host'].";port=".$db['port'].";dbname=".$db['dbname'].";charset=utf8",
        $db['user'],
        $db['pass'],
        [
          PDO::ATTR_EMULATE_PREPARES => true,
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
      );
    }

    return self::$instance;
  }
}
