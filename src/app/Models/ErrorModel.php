<?php

namespace CDN\Models;


/**
 * @SWG\Definition(
 *   required={"code", "message"},
 *   type="object"
 * )
 */
class ErrorModel
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $id;

  /**
   * @SWG\Property(
   *   format="int32"
   * )
   * @var integer
   */
  public $code;

  /**
   * @SWG\Property()
   * @var string
   */
  public $message;

  /**
   * @SWG\Property()
   * @var string
   */
  public $file;

  /**
   * @SWG\Property()
   * @var string
   */
  public $line;
}
