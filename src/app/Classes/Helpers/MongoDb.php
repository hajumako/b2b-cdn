<?php
/**
 * @file
 * Contains class MongoDb code implementation.
 */

namespace CDN\Classes\Helpers;


use MongoDB\Client;
// https://docs.mongodb.com/php-library/current/tutorial/crud/#find-many-documents

class MongoDb
{
  const COLLECTION_ORDERS = 'orders';
  const COLLECTION_LINE_ITEMS = 'line_items';
  const COLLECTION_PAYMENTS = 'payments';
  const COLLECTION_PAYMENTS_PAYU = 'payments_payu';
  const COLLECTION_ORDER_STATUS = 'order_status';
  const COLLECTION_COUNTRY_REGIONS = 'country_regions';

  const COLLECTION_ACCESSORY_SPECS = 'accessory_specs';
  const COLLECTION_COMMODITY_SPECS = 'commodity_specs';
  const COLLECTION_HEATER_SPECS = 'heater_specs';
  const COLLECTION_RADIATOR_SPECS = 'radiator_specs';

  const COLLECTION_RADIATOR_COLORS = 'radiator_colors';
  const COLLECTION_RADIATOR_CONFIGS = 'radiator_configs';
  const COLLECTION_LAST_UPDATE = 'last_update';
  const COLLECTION_CACHE_INFO_LOG = 'cache_info_log';

  private static $client = null;
  private static $instance = null;

  private function __construct()
  {
  }

  final public static function getConnection(array $db)
  {
    if (!self::$instance) {
      $conString = "mongodb://".$db['user'].":".$db['pass']."@".$db['host'];
      // Connect to Mongo with default setting.
      self::$client = new Client($conString);
      // Connect to database.
      self::$instance = self::$client->selectDatabase($db['dbname']);
    }

    return self::$instance;
  }
}
