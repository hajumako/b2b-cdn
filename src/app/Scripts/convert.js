const
  argv = require('minimist')(process.argv.slice(2)),
  fs = require('fs'),
  path = require('path'),
  moment = require('moment'),
  makeDir = require('make-dir'),
  probe = require('probe-image-size'),
  sharp = require('sharp'), // https://github.com/lovell/sharp
  imagemin = require('imagemin'),
  webp = require('imagemin-webp'),
  pngquant = require('imagemin-pngquant'), // https://github.com/imagemin/imagemin-pngquant

  _dLogFormat = 'YYMMDD',
  _dConsoleFormat = 'YYYY-MM-DD HH:mm:ss',
  _logPath = '../../logs/convert/',
  _defBg = {
    r: 255,
    g: 255,
    b: 255,
    alpha: 1
  },
  format = {
    png: 'png',
    webp: 'webp',
    jpg: 'jpg',
    jpeg: 'jpeg'
  };

let
  cnt = 0,
  log,
  _s = moment(),
  _logName = 'convertLog_' + _s.format(_dLogFormat);

if (argv.hasOwnProperty('src') && argv.hasOwnProperty('dest') && argv.hasOwnProperty('format')) {

  if (argv.hasOwnProperty('width')) {
    if (typeof argv.width !== 'number' && (argv.width % 1) !== 0) {
      if (!argv.hasOwnProperty('silent')) {
        console.log('\'width\' is not a number');
      }
      return;
    }
  }

  if(!format.hasOwnProperty(argv.format)) {
    if (!argv.hasOwnProperty('silent')) {
      console.log('uknown \'format\'');
    }
    return;
  }

  argv.src = path.normalize(argv.src);
  argv.dest = path.normalize(argv.dest);

  makeDir(path.normalize(_logPath))
    .then(() => {
      log = {
        dateStarted: _s.format(),
        src: argv.src,
        dest: argv.dest,
        format: argv.format,
        width: argv.width
      };
    });

  if (!argv.hasOwnProperty('silent')) {
    console.log('----------------------');
    console.log('started', _s.format(_dConsoleFormat));
    console.log('----------------------');
  }

  makeDir(argv.dest)
    .then(() => {
      if (argv.width) {
        if (!argv.hasOwnProperty('silent')) {
          console.log('convert & resize');
        }
        return probe(fs.createReadStream(argv.src));
      } else {
        if (!argv.hasOwnProperty('silent')) {
          console.log('convert');
        }
        return Promise.resolve();
      }
    })
    .then(data => {
      let height = null;
      if (argv.width) {
        height = Math.round(argv.width / data.width * data.height);
      }
      switch (argv.format) {
        case format.png:
          return convertToPng(argv.src, argv.dest, argv.width, height);
        case format.webp:
          return convertToWebp(argv.src, argv.dest, argv.width, height);
        case format.jpg:
        case format.jpeg:
          return convertToJpeg(argv.src, argv.dest, argv.width, height);
      }
    })
    .then(data => {
      if (argv.format === format.png) {
        if (!argv.hasOwnProperty('silent')) {
          console.log('compress png');
          console.log(path.join(argv.dest, path.parse(argv.src).base));
        }
        return imagemin([path.join(argv.dest, path.parse(argv.src).base)], argv.dest, {
          plugins: [
            pngquant()
          ]
        });
      } else {
        return data;
      }
    })
    .then((data) => {
      log.generated = data.length ? data[0].path : '----';
    })
    .then(() => {
      finish();
    })
    .catch(e => {
      if (!argv.hasOwnProperty('silent')) {
        console.log(e);
      }
    });
} else {
  if (!argv.hasOwnProperty('silent')) {
    if (!argv.hasOwnProperty('src')) {
      console.log('missing \'src\' argument');
    }
    if (!argv.hasOwnProperty('dest')) {
      console.log('missing \'dest\' argument');
    }
    if (!argv.hasOwnProperty('format')) {
      console.log('missing \'format\' argument');
    }
  }
}

function convertToPng(file, dest, width = null, height = null) {
  if (fs.lstatSync(dest).isDirectory()) { // destination is a directory; add file name to path
    dest = path.join(dest, path.parse(file).name + '.png');
  }
  if (width && height) {
    return sharp(file)
      .resize(width, height)
      .toFile(dest);
  } else {
    return sharp(file)
      .toFile(dest);
  }
}

function convertToJpeg(file, dest, width = null, height = null) {
  if (fs.lstatSync(dest).isDirectory()) { // destination is a directory; add file name to path
    dest = path.join(dest, path.parse(file).name + '.jpg');
  }
  let bg = argv.hasOwnProperty('bg') ? (hexToRgb(argv.bg) || _defBg) : _defBg;
  if (width && height) {
    return sharp(file)
      .resize(width, height)
      .background(bg)
      .flatten()
      .toFile(dest);
  } else {
    return sharp(file)
      .background(bg)
      .flatten()
      .toFile(dest);
  }
}

function convertToWebp(file, dest, width = null, height = null) {
  if (!fs.lstatSync(dest).isDirectory()) { // destination is not a directory; remove file name from path
    dest = path.dirname(dest);
  }
  let opts = {
    method: 6,
    nearLossless: 90
  };
  if (width && height) {
    opts.resize = {
      width,
      height
    }
  }
  // TODO problem!
  // TODO if generated webp file is bigger tham png then script saves file as png
  return imagemin([file], dest, {
    plugins: [
      webp(opts)
    ]
  });
}

function hexToRgb(hex) {
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
    alpha: 1
  } : null;
}

function saveLog() {
  fs.readFile(path.normalize(_logPath + _logName + '.json'), (err, data) => {
    let _log = [];
    if (err) {
      if (!argv.hasOwnProperty('silent')) {
        //console.log(err);
      }
    } else {
      _log = JSON.parse(data);
    }
    _log.push(log);
    fs.writeFile(
      path.normalize(_logPath + _logName + '.json'),
      JSON.stringify(_log, null, 2),
      (err) => {
        if (err) throw err;
      });
  });
}

function finish() {
  let _f = moment();
  log.dateFinished = _f.format();
  log.duration = _f.diff(_s, 'milliseconds') + 'ms';
  saveLog();

  if (!argv.hasOwnProperty('silent')) {
    console.log(log);
    console.log('----------------------');
    console.log('finished', moment().format(_dConsoleFormat));
    console.log('duration', log.duration);
    console.log('----------------------');
  }
}