<?php
/**
 * @file
 * Contains class Model code implementation.
 */

namespace CDN\Models;

use CDN\Classes\Helpers\MariaDb;
use CDN\Classes\Helpers\MongoDb;
use DI\Container;
use MongoDB\BSON\ObjectID;
use MongoDB\InsertOneResult;
use MongoDB\UpdateResult;
use PDO;
use PDOException;
use Psr\Http\Message\ServerRequestInterface as Request;
use ReflectionClass;
use stdClass;

class Model
{
  protected $container;
  protected $db;
  protected $tokenController;
  protected $uid;
  protected $total;
  protected $logger;
  protected $mongoDb;
  protected $ctorArgs = null;

  /**
   * Model constructor.
   *
   * @param \DI\Container $container
   */
  public function __construct(Container $container)
  {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->db = MariaDb::getConnection($this->container->get('db'));
    $this->mongoDb = MongoDb::getConnection($this->container->get('mongodb'));
  }

  /**
   * Get total number of records
   *
   * @return int
   *   Total number of records
   */
  public function getTotal()
  {
    return $this->total;
  }

  /**
   * Get single item from db.
   *
   * @param string $statement
   *   Mysql query
   * @param string|null $fetch_class
   *   Class name which result should be casted to
   *
   * @return object|stdClass
   *   Item object.
   *
   * @throws \ReflectionException
   */
  protected function getItem(string $statement, string $fetch_class = null)
  {
    //TODO more elegant solution needed; used when procedure returns nothing
    try {
      $query = $this->db->query($statement);
      if ($fetch_class && class_exists($fetch_class)) {
        $query->setFetchMode(PDO::FETCH_CLASS, $fetch_class, $this->ctorArgs);
      } else {
        $query->setFetchMode(PDO::FETCH_OBJ);
      }
      $items = $query->fetchAll();
      $items = $this->castingVars($query, $items);
      $query->closeCursor();
      if (count($items)) {
        return $items[0];
      }
    } catch (PDOException $e) {
      if ($this->container->get('debug_config')['log_exceptions'] ?? true) {
        $this->logger->addError($e);
      }
    }

    if ($fetch_class && class_exists($fetch_class)) {
      $reflection_class = new ReflectionClass($fetch_class);
      return $reflection_class->newInstanceArgs($this->ctorArgs);
    }

    return new stdClass();
  }

  /**
   * Method to get query results from database.
   *
   * @param string $statement
   *   Mysql query.
   *
   * @return array
   */
  protected function fetchItems(string $statement)
  {
    $items = [];
    try {
      $query = $this->db->query($statement);
      $items = $query->fetchAll(PDO::FETCH_OBJ);
      $query->closeCursor();
    } catch (PDOException $e) {
      if ($this->container->get('debug_config')['log_exceptions'] ?? true) {
        $this->logger->addError($e);
      }
    }

    return $items;
  }

  /**
   * Set item with supplied query.
   *
   * @param string $statement
   *   Mysql query
   */
  protected function setItem(string $statement)
  {
    $query = $this->db->query($statement);
    $query->closeCursor();
  }

  /**
   * Get items from database with limit & offset and cast to specified object class
   *
   * @param string $statement
   *   SQL statement.
   * @param Request $request
   *   Psr request.
   * @param string|null $fetch_class
   *
   * @return array
   *   Array of items retrieved from db.
   */
  protected function getItems(string $statement, Request $request = null, string $fetch_class = null): array
  {
    $limited = false;
    if ($request) {
      $params = $request->getQueryParams();
      if (count($params)) {
        foreach ($params as $name => $value) {
          if ($name === 'filter') {
            $s = " WHERE ";
            $filters = explode(',', $value);
            foreach ($filters as $i => $filter) {
              $f = explode('.', $filter);
              switch ($f[1]) {
                case 'eq':
                  $s .= "$f[0] = '$f[2]' ";
                  break;
                case 'gte':
                  $s .= "$f[0] >= '$f[2]' ";
                  break;
                case 'gt':
                  $s .= "$f[0] > '$f[2]' ";
                  break;
                case 'lte':
                  $s .= "$f[0] <= '$f[2]' ";
                  break;
                case 'lt':
                  $s .= "$f[0] < '$f[2]' ";
                  break;
              }
              if ($i < count($filters) - 1) {
                $s .= "AND ";
              }
            }
            $statement .= $s;
          } elseif ($name === 'order') {
            $s = " ORDER BY ";
            $orders = explode(',', $value);
            foreach ($orders as $i => $order) {
              $o = explode('.', $order);
              switch ($o[1]) {
                case 'asc':
                  $s .= "$o[0] ASC ";
                  break;
                case 'desc':
                  $s .= "$o[0] DESC ";
                  break;
              }
              if ($i < count($orders) - 1) {
                $s .= ", ";
              }
            }
            $statement .= $s;
          } elseif ($name === 'limit' || $name === 'offset') {
            $statement .= " " . $name . " " . $value;
            $limited = true;
          }
        }
      }
      if ($limited) {
        $statement = str_replace("SELECT", "SELECT SQL_CALC_FOUND_ROWS", $statement);
      }
    }

    //TODO more elegant solution needed; used when procedure returns nothing
    try {
      $query = $this->db->query($statement);
      if ($fetch_class && class_exists($fetch_class)) {
        $query->setFetchMode(PDO::FETCH_CLASS, $fetch_class, $this->ctorArgs);
      } else {
        $query->setFetchMode(PDO::FETCH_OBJ);
      }
      $items = $query->fetchAll();
      $items = $this->castingVars($query, $items);
      $query->closeCursor();
      if ($limited) {
        $t = $this->db->query("SELECT FOUND_ROWS()")->fetchAll();
        if (count($t)) {
          $this->total = $t[0]['FOUND_ROWS()'];
        }
      }

      return $items ?? array();
    } catch (PDOException $e) {
      if ($this->container->get('debug_config')['log_exceptions'] ?? true) {
        $this->logger->addError($e);
      }
    }

    return array();
  }

  /**
   * Method to casting values from database to proper data types.
   *
   * TODO: Check if this could be done with parameter:
   * TODO: "PDO::ATTR_EMULATE_PREPARES => false",
   * TODO: because there was an mysql issue if procedure is called more than once: server gone away...
   *
   * @param object $query
   *   Query object.
   * @param $items
   *   Array of query result items.
   *
   * @return mixed
   */
  protected function castingVars($query, $items)
  {
    $meta = array();
    foreach (range(0, $query->columnCount() - 1) as $column_index) {
      $item = $query->getColumnMeta($column_index);
      $metas[] = $item;
      switch ($item['native_type']) {
        case 'LONG':
        case 'TINY':
          $type = 'int';
          break;
        case 'NEWDECIMAL':
        case 'DECIMAL':
          $type = 'float';
          break;
        default:
          $type = 'string';
          break;
      }
      $meta[$item['name']] = $type;
    }

    foreach ($items as $key => $item) {
      foreach ($item as $k => &$v) {
        if (array_key_exists($k, $meta)) {
          settype($v, $meta[$k]);
        }
      }
    }

    return $items;
  }

  /**
   * Method to save document in specific mongoDb collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param mixed $document
   *   Document to record.
   *
   * @return InsertOneResult
   */
  public function setCollectionDoc(string $collection, $document): InsertOneResult
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->insertOne($document);
  }

  /**
   * Method to remove document from specific mongoDb collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param string $oid
   *   Document identifier.
   *
   * @return \MongoDB\DeleteResult
   */
  public function removeCollectionDoc(string $collection, string $oid)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->deleteOne(['_id' => new ObjectID($oid)]);
  }

  /**
   * Method to find one document in specific collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param array $conditions
   *   Array of searching conditions.
   *
   * @return array|null|object
   */
  public function getCollectionDoc(string $collection, array $conditions)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->findOne($conditions);
  }

  /**
   * Method to find all documents in specific collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param array $conditions
   *   Array of searching conditions.
   *
   * @return array|null|object
   */
  public function getCollectionDocs(string $collection, array $conditions)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->find($conditions);
  }

  /**
   * Method to update existing document or insert new if conditions not met
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param array $conditions
   *   Array of searching conditions.
   * @param $document
   *   Data to save/update
   *
   * @return UpdateResult
   */
  public function upsertCollectionDoc(string $collection, array $conditions, $document): UpdateResult
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->updateOne(
      $conditions,
      ['$set' => $document],
      ['upsert' => true]
    );
  }

}
