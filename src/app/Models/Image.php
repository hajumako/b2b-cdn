<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 12.01.2018
 * Time: 11:57
 */

namespace CDN\Models;


use DI\Container;

// TODO move to 'models' - class reflects data returned from db
class Image
{
  public $entityId;
  public $modelAX;
  public $type;
  public $uri;
  public $ext;
  public $srcset;

  protected $imgHost;
  protected $cdnHost;
  protected $ctorArgs;

  public function __construct(Container $container)
  {
    $this->imgHost = $container->get('img_host');
    $this->cdnHost = $container->get('cdn_host');
    $this->ctorArgs = $container;
  }

  public function prepareImage(): Image
  {
    $this->uri = preg_replace("/ /", "%20", str_replace('public://', $this->imgHost, $this->uri));
    $arr = explode('.', $this->uri);
    $this->ext = $arr[count($arr) - 1];

    return $this;
  }
}