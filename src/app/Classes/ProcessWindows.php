<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 29.03.2018
 * Time: 12:55
 */

namespace CDN\Classes;


use Monolog\Logger;
use Symfony\Component\Process\Process;

class ProcessWindows extends Process
{

  protected $process;
  protected $contents;
  protected $logger;
  protected $proceErrorLogPath;

  /**
   * ProcessWindows constructor.
   *
   * @param $commandline
   * @param \Monolog\Logger $logger
   * @param null|string $cwd
   * @param array|null $env
   * @param null|mixed $input
   * @param int $timeout
   */
  public function __construct(
    $commandline,
    Logger $logger,
    string $procErrorLogPath,
    ?string $cwd = null,
    ?array $env = null,
    ?mixed $input = null,
    $timeout = 60
  ) {
    $this->logger = $logger;
    $this->proceErrorLogPath = $procErrorLogPath;
    parent::__construct($commandline, $cwd, $env, $input, $timeout);
  }

  public function runWindows($input = null): int
  {
    if ('\\' === DIRECTORY_SEPARATOR) {
      $descriptorspec = [
        0 => ["pipe", "r"],  // stdin is a pipe that the child will read from
        1 => ["pipe", "w"],  // stdout is a pipe that the child will write to
        2 => ["file", $this->proceErrorLogPath . "proc_error.txt", "a"] // stderr is a file to write to
      ];

      $commandline = parent::getCommandLine();
      $process = proc_open($commandline, $descriptorspec, $pipes, null, null, ['bypass_shell' => true]);

      if (is_resource($process)) {
        if ($input) {
          // writeable handle connected to child stdin
          fwrite($pipes[0], $input);
          fclose($pipes[0]);
        }

        // readable handle connected to child stdout
        $this->contents = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        return proc_close($process);
      }

      return -1;
    } else {
      $callback = function ($output, $outputMsg) {
        $this->logger->addInfo($output . ':' . $outputMsg);
        $this->contents .= $outputMsg;
      };
      $result = parent::run($callback);
      $this->logger->addInfo($this->getExitCode() . ': ' . $this->getExitCodeText());

      return $result;
    }
  }

  public function getContents()
  {
    return $this->contents;
  }
}