<?php

namespace CDN\Classes\Exceptions;

use B2B\Classes\ResponseCode;
use Exception;
use Throwable;

/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 01.03.2018
 * Time: 12:34
 */

class ForbiddenException extends Exception
{
  public function __construct($message = "", Throwable $previous = null)
  {
    parent::__construct($message, ResponseCode::FORBIDDEN, $previous);
  }
}